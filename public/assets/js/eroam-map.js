// EROAM MAP MODULE


var Map = (function() {

	var map,
		settings = {
			radius: 400000,
			displayRadius: false
		},
		theme = {
			styles: [
				{'featureType': 'administrative.province', 'elementType': 'labels', 'stylers': [{'visibility': 'off'}]},
				{'featureType': 'administrative.neighborhood', 'elementType': 'labels', 'stylers': [{'visibility': 'off'}]},
				{'featureType': 'administrative.locality', 'elementType': 'labels', 'stylers': [{'visibility': 'off'}]},
				{'featureType': 'landscape', 'stylers': [{'color': '#EAEAEA'}]}, // landscape
				{'featureType': 'road', 'stylers': [{'color': '#d8d8d8'}]}, // road
				{'featureType': 'water', 'stylers': [{'color': '#FFFFFF'}]}, // water
				{'featureType': 'poi', 'stylers': [{'color': '#EAEAEA'}]} // point of interest
			],
			icon: {
				active: siteUrl('assets/images/map/eroam-pin-blue.png'),
				inactive: {
					path: google.maps.SymbolPath.CIRCLE,
					strokeColor: '#2AA9DF',
					strokeOpacity: 0.5,
					fillOpacity: 1,
					fillColor: '#2AA9DF',
					scale: 4
				}
			},
			polyline: {
				flightFerry: {
					strokeOpacity: 1,
					strokeColor:'#27A8DF',
					strokeWeight: 3,
					geodesic: true
				},
				noRoute: {
					strokeOpacity: 1,
					strokeColor:'#969696',
					strokeWeight: 3,
					geodesic: true
				}
			},
			route: {
				active: {
					strokeOpacity: 1,
					strokeColor:'#27A8DF',
					strokeWeight: 3,
					geodesic: true
				}
			}
		},
		dirService = new google.maps.DirectionsService;

		function init(mode = 'create') {
			map = new google.maps.Map(document.getElementById('map'), {
				zoom: 5,
				styles : theme.styles,
				mapTypeControl: false
			});

			google.maps.event.addListener(map, 'zoom_changed', function() {
				if (this.getZoom() > 5) {
					// SHOW CITY lABELS
					$.each(map.markers, function(k, v) {
						v.label.open(map);
					});
				} else {
					// HIDE CITY lABELS
					$.each(map.markers, function(k, v) {
						v.label.close();
					});
				}
			});

			// AN ARRAY FOR ALL MARKERS
			map.markers = [];

			// AN ARRAY FOR DIRECTION ROUTES
			map.directionsRoutes = [];

			// IF VIEWING
			if (mode == 'view') {
				var routes = JSON.parse($('#routes').val());
				// GET THE DEFAULT ROUTES
				var default_route = [];
				var found = routes.some(function(v, k) {
					return v.is_default == 1;
				});
				if (found) {
					$.grep(routes, function(v, k) {
						if (v.is_default == 1) {
							default_route = v;
						}
					});
					viewRoute(default_route);
				}
			}

			console.log('Map Initialized');
			map.setCenter({
				lat: -25.2744,
				lng: 133.7751
			});
		}

		function viewRoute(route) {
			// RESET MAP
			$.each(map.directionsRoutes, function(key, value) {
				value.setMap(null);
			});
			map.directionsRoutes = [];
			$.each(map.markers, function(key, value) {
				value.setMap(null);
			});
			map.markers = [];


			$.each(route.route_legs, function(key, value) {
				var marker = addMarker(value.city);
				setCenter(value.city);
				if (key >= 1) {
					setRoute(marker);
				}
			});
		}

		function resetRoutes() {
			$.each(map.directionsRoutes, function(key, value) {
				value.setMap(null);
			});
			map.directionsRoutes = [];

			$.each(map.markers, function(key, value) {
				if (key != 0) {
					if (value.roundTrip) {
						setRoute(value, null, true, key);
					} else {
						setRoute(value);
					}
				} else if (key == 0 && map.markers.length == 1) { // if 1 marker is left
					setCenter(value.city);
				}
			});
		}

		 function addLocation(city) {
			setCenter(city);

			// ADD MARKER
			var marker = addMarker(city);
			if (map.markers.length > 1) {
				setRoute(marker);
			}
		}

		function removeLocation(index) {
			map.markers[index].setMap(null);
			map.markers.splice(index, 1);
			resetRoutes();
		}

		function setCenter(obj) {
			map.setCenter({
				lat: parseFloat(obj.latlong.lat),
				lng: parseFloat(obj.latlong.lng)
			});
		}

		function addMarker(obj) {
			var marker = new google.maps.Marker({
				position: latlng(obj.latlong.lat, obj.latlong.lng),
				map: map,
				icon: theme.icon.active,
			});

			// city label
			setInfoBox(marker, obj);

			marker.city = obj;
			marker.cityId = obj.id;
			map.markers.push(marker);

			var infowindowCity = new google.maps.InfoWindow({
				disableAutoPan: true
			});
			marker.infowindowCity = infowindowCity;

			if ($('#city-label-' + obj.id).length == 0) {
				infowindowCity.setContent('<div id="city-label-' + obj.id + '"  class="city-label">' + obj.name + '</div>');
				infowindowCity.setZIndex(-999);
			}

			google.maps.event.addListener(infowindowCity, 'domready', function() {
				$('.city-label').parent().parent().parent().addClass('city-label-outer');
				$('.city-label').parent().parent().parent().parent().addClass('city-label-outer-parent');
				$('.city-label').parent().parent().parent().parent().on('mousedown touchstart', function(event) {
					event.preventDefault();
				});
				$('.city-label').parent().parent().parent().prev().find('> :nth-child(2)').css('top', '8px');
				$('.city-label').parent().parent().parent().prev().find('> :nth-child(4)').css('top', '8px');
				$('.city-label').parent().parent().parent().next().remove();
			});

			// add event listener on hover
			marker.addListener('mouseover', function() {
				marker.infowindowCity.open(map, this);
			});
			// add event listener on hover
			marker.addListener('mouseout', function() {
				marker.infowindowCity.close();
			});

			return marker;
		}

		function setInfoBox(marker, obj) {
			var infoboxOptions = {
				content: '<div>' +  obj.name + '</div>',
				boxStyle: {
					textAlign: 'left',
					whiteSpace: 'nowrap',
					lineHeight: '16px',
					zIndex: '-999',
					margin: '-7px 0 0 35px' // RIGHT = -18px 0 0 24px, LEFT = 8px 0 0 -134px
				},
				disableAutoPan: true,
				pixelOffset: new google.maps.Size(-25, 0),
				position: marker.getPosition(),
				closeBoxURL: "",
				isHidden: false,
				pane: "floatPane",
				enableEventPropagation: true
			};

			var label = new InfoBox(infoboxOptions);
			marker.label = label;
		}

		function latlng(lat, lng) {
			return new google.maps.LatLng(lat, lng);
		}

		function setRoute(marker) {
			var prevMarker = map.markers[map.markers.indexOf(marker) - 1],
				path = [
					{ lat: prevMarker.getPosition().lat(), lng: prevMarker.getPosition().lng() },
					{ lat: marker.getPosition().lat(), lng: marker.getPosition().lng() }
				],
				routeOptions = {
					origin: latlng(path[0].lat, path[0].lng),
					destination: latlng(path[1].lat, path[1].lng),
					travelMode: google.maps.TravelMode.DRIVING,
					optimizeWaypoints: false
				},
				data = {
					from_city_id: prevMarker.cityId,
					to_city_id: marker.cityId
				};
                                fitMarkersToMap();
				$.ajax({
					method: 'post',
					url: siteUrl('route/get-transport-by-city'),
					data: data,
					success: function(response) {
						var flightOrFerry = response.data.length > 0 && response.data[0].transport_type_id == 1 ? true : false;
						var route = function(fix = false) {
							dirService.route(routeOptions, function(response, status) {
								if (status === google.maps.DirectionsStatus.OK && !flightOrFerry) {
									// draw actual route
									drawRoute(response, false, fix, prevMarker, marker);
								} else if (flightOrFerry) {
									// draw flight or ferry polyline
									drawPolyline(path, true, false, fix, prevMarker, marker);
								} else if (status == 'ZERO_RESULTS') {
									// draw polyline for missing routes or own arrangement
									drawPolyline(path, false, false, fix, prevMarker, marker);
								} else if (status == 'OVER_QUERY_LIMIT') {
									setTimeout(function() {
										route(true);
									}, 100);
								}
							});
						};
						route();
					},
					beforeSend: function() {
						$('#map-loader').show();
					},
					complete: function() {
						fitMarkersToMap();
					}
				});
		}

		function drawRoute(directions, alternative, fix = false, origin = null, destination = null) { // draw the actual route
			var paths = [];
			var steps = directions.routes[0].legs[0].steps;
			$.each(steps, function(key, value) {
				$.each(value.path, function(k, v) {
					paths.push(v);
				});
			});
			// CREATE THE POLYLINE
			var polyline = new google.maps.Polyline(theme.route.active);
			polyline.setPath(paths);
			if (!alternative) {
				polyline.origin = origin.getPosition();
				polyline.destination = destination.getPosition();
				polyline.from_city_id = origin.cityId;
				polyline.to_city_id = destination.cityId;
			}
			polyline.setMap(map);
			map.directionsRoutes.push(polyline);
		}

		function drawPolyline(path, hasRoute, alternative, fix = false, origin = null, destination = null) { // draw the actual polyline
			var line = hasRoute ? theme.polyline.flightFerry : theme.polyline.noRoute;
			if (alternative) {
				line = theme.route.alternative;
			}
			var polyline = new google.maps.Polyline(line);
			polyline.setPath(path);
			if (!alternative) {
				polyline.origin = origin.getPosition();
				polyline.destination = destination.getPosition();
				polyline.from_city_id = origin.cityId;
				polyline.to_city_id = destination.cityId;
			}
			polyline.setMap(map);
			map.directionsRoutes.push(polyline);
			var distance = google.maps.geometry.spherical.computeDistanceBetween(latlng(path[0].lat, path[0].lng), latlng(path[1].lat, path[1].lng));
			// console.log(distance / 1000 + ' kilometers');

		}

		function fitMarkersToMap() {
			var bounds = new google.maps.LatLngBounds();
			$.each(map.markers, function(k, v) {
				bounds.extend(v.getPosition());
			});

			google.maps.event.addListener(map, 'zoom_changed', function() {
				var listener = google.maps.event.addListener(map, 'bounds_changed', function(event) {
					if (this.getZoom() > 5 && this.initialZoom === true) {
						this.setZoom(5);
						this.initialZoom = false;
					}

					google.maps.event.removeListener(listener);
				});
			});
			map.initialZoom = true;
			map.fitBounds(bounds);

			$('#map-loader').fadeOut();
		}

		function getMap() {
			return map;
		}

	return {
		init: init,
		get: getMap,
		addLocation: addLocation,
		removeLocation: removeLocation,
		viewRoute: viewRoute
	};

})();