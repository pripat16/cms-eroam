<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZAgents extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('zAgents')){  
            Schema::create('zAgents', function($table)
            {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->string('address',255)->nullable();
                $table->string('contact_number',50)->nullable();
                $table->string('image_url')->nullable();
                $table->integer('agent_commission_percentage_id')->unsigned()->nullable();
                $table->timestamps();
                $table->softDeletes();
                
                $table->foreign('user_id')->references('id')->on('users');
                //$table->foreign('agent_commission_percentage_id')->references('id')->on('zAgentCommissionPercentages');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zAgents');
    }
}
