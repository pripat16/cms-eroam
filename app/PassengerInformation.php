<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PassengerInformation extends Model
{
    protected $fillable = [
                            'itenary_order_id', 'title','first_name','last_name','email','contact','gender','country','dob','passport_expiry_date','passport_num',
                            'address_one','address_two','suburb','state','zip','is_lead'
                        ];
    protected $table = 'passengerinformations';
    protected $primaryKey = 'passenger_information_id';
    
    public function Country()
    {
        return $this->hasMany('App\Country', 'id', 'country');
    }
}
