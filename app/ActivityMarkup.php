<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityMarkup extends Model
{
    protected $table = 'zactivitymarkups';
    protected $primaryKey = 'id';
    protected $fillable = ['name','description','allocation_type','allocation_id','is_default','is_active','activity_markup_percentage_id'
                            ,'activity_markup_agent_commission_id','activity_markup_supplier_commission_id'];
    
    public function markup_percentage(){
        return $this->hasOne('App\ActivityMarkupPercentage','activity_markup_id','id');
    }
}
