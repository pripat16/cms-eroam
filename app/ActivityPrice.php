<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityPrice extends Model
{
    protected $fillable = [
        'activity_id','allotment','release','minimum_pax','operates','name','date_from','date_to','activity_base_price_id',
        'activity_markup_id','activity_markup_percentage_id','activity_supplier_id','cancellation_policy','cancellation_formula','currency_id'
    ];
    protected $table = 'zactivityprices';
    protected $primaryKey = 'id';
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    public function currency(){
        return $this->hasOne('App\Currency','id','currency_id');
    }
    public function supplier(){
        return $this->hasOne('App\ActivitySupplier','id','activity_supplier_id');
    }
}
