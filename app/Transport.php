<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\TransportPrice;
use App\Currency;
use App\TransportOperator;

class Transport extends Model
{
    protected $fillable = [
        'from_city_id','to_city_id','via','transport_type_id','currency_id','default_transport_supplier_id','operator_id'
        ,'[default]','is_international','etd','eta','phone','voucher_comments','notes','address_from',
        'address_to','arrives_on','is_publish'
    ];
    protected $table = 'ztransports';
    protected $primaryKey = 'id';
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    public function transporttype(){
        return $this->hasOne('App\TransportType','id','transport_type_id');
    }
    
    public function operator(){
        return $this->hasOne('App\TransportOperator', 'id', 'operator_id');
    }

    public function price(){
        return $this->hasMany('App\TransportPrice', 'transport_id', 'id')->with('currency', 'supplier', 'passenger');
    }
    public function currency(){
        return $this->hasOne('App\Currency','id','currency_id');
    }
}
