<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelCategory extends Model
{
    protected $table = 'zhotelcategories';
    protected $fillable = [
        'name','category','sequence','hotel_stars','is_default'
    ];
    protected $primaryKey = 'id';
}
