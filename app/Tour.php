<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\City;
use App\Country;

class Tour extends Model
{
    protected $fillable = [
        'code','tour_code','tour_title','tour_url','discount','destination','saving_per_person','departure','tour_type_logo_id','start_date','end_date'
        ,'no_of_days_text','no_of_days','durationType','price','short_description','transport','long_description','grade','region_id','groupsize_min'
        ,'groupsize_max','grouptext','mapfile','food','accommodation','travel_guide','additional_info','provider','is_active','_year','url','tripStyle'
        ,'brochureSupplier','tripCountries','tripActivities','tripRegion','serviceLevel','RBCode','LastUpdate','is_sync','is_childAllowed','children_age'
        ,'Date_LastUpdate','views','meta_title','meta_description','meta_keywords','tour_currency','tour_PracticalDetail','IsLive_PracticalDetail'
        ,'xml_PracticalDetail','xml_itinerary','is_reviewed','is_approve','sync_error','tour_remarks','is_deleted','delete_by','id_delete_by','updated_by'
        ,'admin_id_updated','date_admin_updated','agent_id_updeted','date_agent_updated','provider_id_updated','date_provider_updated','updated_msg'
        ,'provider_tour_id','from_city_id','to_city_id','operator_id','supplier_id','de_countries','AdultPriceSingle','ChildPriceSingle','AdultSupplement'
        ,'ChildPrice','retailcost','flightPrice','flight_currency_id','flightDescription','flightReturn','flightDepart','countryData'
    ];
    protected $table = 'tbltours';
    protected $primaryKey = 'tour_id';
    
    public function currency(){
        return $this->hasOne('App\Currency','code','tour_currency');
    }
    
    public static function getTourList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10)
    {
        $oTours = Tour::from('tbltours as t')
                        ->select('tour_code','tour_id','tour_title','no_of_days_text','price'
                                ,'provider_name','t.is_active','t.is_reviewed'
                                ,'views','destination','departure')
                        ->join('tblProviders as p', 't.provider', '=', 'p.provider_id')
                        ->where('t.is_deleted','=', 0)
                        ->orderBy($sOrderField, $sOrderBy);
                        
        if($sSearchBy == 'city' || $sSearchBy == 'country')
        {
            
            $oTours->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                        if($sSearchBy == 'city'){
                            $query->where('t.departure', 'like', '%' . $sSearchStr . '%')
                                  ->orWhere('t.destination', 'like', '%' . $sSearchStr . '%');
                        }
                        else {
                            $country_list = Country::where('name', 'LIKE', '%'.$sSearchStr.'%')->pluck('id');
                            $city_list    = City::wherein('country_id', $country_list)->pluck('name');
                            
                            $query->wherein('departure', $city_list)
                                ->orwherein('destination', $city_list);
                        }
                    });
        }
        else
            $oTours->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        });
                        
        return $oTours->paginate($nShowRecord);
    }
}