<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingRequest extends Model
{
    protected $fillable = [
                            'tour_id', 'tour_title','Title','FName','SurName','gender','DOB','email','address', 'nationality','departure_date',
                            'return_date','provider','request_date','status','singletotal','singletotal','twintotal','price', 'Agent_id','code',
                            'contact','RequestTextXML','RequestTextReisebazaar','BookingType','is_specialOffer','payment_method','credit_card_type',
                            'credit_card_no','CVV_no','card_holder_name','card_holder_address','card_validity','is_ch_travling','eway_result','eway_authcode',
                            'eway_error','eway_transaction_no','eway_txn_number','eway_option1','eway_option2','eway_option3','passport_pic','credircard_pic1',
                            'creditcard_pic2','sub_total','addon_total_price','credit_card_fee','grand_total','booking_currency','voucher_sent','domain_id',
                            'voucherNo','refferenceNo','joiningInstruction','notes','postal_code','supplier_notes','tour_notes','custom_notes','commission_date',
                            'is_commission_paid','supplier_paid','manual_booking_id','domain','postal_code','supplier_notes','tour_notes','custom_notes','commission_date',
                            
                        ];
    protected $table = 'tblbookingrequests';
    protected $primaryKey = 'request_id';
    public $timestamps = false;
    
    public static function getTourBooking($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10)
    {
        return BookingRequest::from('tblbookingrequests as b')
                                ->select('b.*')
                                ->join('tbltours as t', 't.tour_id', '=', 'b.tour_id')
                                ->join('zAgents as a', 'a.id', '=', 'b.Agent_id')
                                ->where('t.is_deleted','=', 0)
                                ->when($sSearchStr, function($query) use($sSearchStr) {
                                        $query->where('b.request_id','LIKE', '%'.$sSearchStr.'%');
                                        $query->orWhere('b.FName','LIKE', '%'.$sSearchStr.'%');
                                        $query->orWhere('t.tour_title','LIKE', '%'.$sSearchStr.'%');
                                })
                                ->orderBy($sOrderField, $sOrderBy)
                                ->paginate($nShowRecord);
    }
    
}
