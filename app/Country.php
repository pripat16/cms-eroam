<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Country extends Model
{
    protected $fillable = [
        'name','continent_id','code','iso_numeric_code','iso_3_letter_code','show_on_eroam','region_id','show_on_eroam'
    ];
    protected $table = 'zcountries';
    protected $primaryKey = 'id';
    
    public function region()
    {
        return $this->belongsTo('App\Region');
    }
    
    public static function geCountryList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10)
    {
        return Country::from('zcountries as c')
                    ->leftJoin('zregions as r','c.region_id','=','r.id')
                    ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where('c.'.$sSearchBy,'like','%'.$sSearchStr.'%');
                        })
                    ->select(
                        'c.id as id',
                        'c.name as name',
                        'c.code as code',
                        'c.show_on_eroam as show_on_eroam',
                        'c.created_at as created_at',
                        'c.updated_at as updated_at',
                        'r.name as region_name'
                        )
                    ->orderBy($sOrderField, $sOrderBy)
                    ->paginate($nShowRecord);
    }
}
