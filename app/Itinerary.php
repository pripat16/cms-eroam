<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Itinerary extends Model
{
     protected $fillable = [
                            'is_booked', 'customer_id','agent_id','reference_no','title','to_city_id','from_city_id','num_of_travellers',
                            'travel_date', 'is_deleted','total_itinerary_legs','total_amount','currency','total_days','total_per_person','total_refundable',
                            'is_saved_itinerary', 'ae_api_response','search_session'
                        ];
    protected $table = 'zitineraries';
    protected $primaryKey = 'id';
    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
