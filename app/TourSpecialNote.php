<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourSpecialNote extends Model
{
    protected $fillable = [
        'special_note_id','tour_id'
    ];
    protected $table = 'tbltourspecialnote';
    protected $primaryKey = 'tour_special_id';
    public $timestamps = false;
}
