<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryDef extends Model
{
    protected $fillable = [
        'category_name','url','is_active','description','meta_title','meta_keywords','meta_desc','activity_image'
    ];
    protected $table = 'tblcategorydef';
    protected $primaryKey = 'category_id';
}
