<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dates extends Model
{
    protected $fillable = [
        'tour_id', 'StartDate','EndDate','AdultPrice','ChildPrice','Availability','adv_purchase','season_id','AdultPriceSingle','ChildPriceSingle'
        ,'AdultSupplement','ChildSupplement'
    ];
    protected $table = 'tbldates';
    protected $primaryKey = 'Date_ID';
    public $timestamps = false;
}
