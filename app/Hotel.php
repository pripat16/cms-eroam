<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $table = 'zhotels';
    protected $fillable = [
        'city_id','optional_city','default_nights','name','currency','currency_id','twin','single','triple','quad','dorm','category',
        'operator','supplier','description','notes','small_image','season','row_id','approved_by_eroam','address','supplier_id','operator_id',
        'category_id','child_rate','short_description','reservation_phone','website','email'
    ];
    protected $primaryKey = 'id';
    
    public function city(){
    	return $this->hasOne('App\City','id','city_id')->with('country');
    }

    public function category(){
    	return $this->hasOne('App\HotelCategory','id','hotel_category_id');
    }

    public function currency()
    {
        return $this->hasOne('App\Currency','id','currency_id');
    }
    
    public static function geHotelList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10)
    {
        return Hotel::from('zhotels as h')
                    ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where('h.'.$sSearchBy,'like','%'.$sSearchStr.'%');
                        })
                    ->orderBy($sOrderField, $sOrderBy)
                    ->with('city','category','currency')
                    ->paginate($nShowRecord);
    }
}
