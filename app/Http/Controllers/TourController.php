<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Country;
use App\City;
use App\Region;
use App\Provider;
use App\CategoryDef;
use App\TourTypeLogo;
use App\Tour;
use App\Currency;
use App\ProviderPickup;
use App\ProviderSpecialNote;
use App\ProviderStandardRemark;
use App\TourStandardRemark;
use App\TourSpecialNote;
use App\TourPickup;
use App\TourCountry;
use App\TourCategory;
use App\LocalPayment;
use App\Season;
use App\Dates;
use App\FlightPayment;
use App\Image as Images;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;

use Session;
use File;
use Image;
use DB;
use Carbon\Carbon;

class TourController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        session(['page_name' => 'tourlist']);
    }
    public function callTourList(Request $oRequest)
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'tourlist' && session('page_name') != 'addtour')
            $oRequest->session()->forget('tour');

        session(['page_name' => 'tourlist']);
        $aData = session('tour') ? session('tour') : array();

        $oRequest->session()->forget('tour');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'tour_id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) && $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        $oTourList = Tour::getTourList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);

        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oTourList->currentPage(),'tour');

        if($oRequest->page > 1)
            $oViewName =  'WebView::tour._more_tour_list';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::tour.tour_list' : 'WebView::tour._tour_list_ajax';
        
        return \View::make($oViewName, compact('oTourList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }
    
    public function callTourCreate(Request $oRequest,$nIdTour = '')
    {
        session(['page_name' => 'addtour']);
        if ($oRequest->isMethod('post'))
        {
            $data  = Input::all();
            $extraData = $data;
            $random_tour_id = substr($data['random_tour_id'],5);
            $allCategories = DB::table('tblCategoryDef')->whereIn('category_id', $data['tripActivities'])->pluck('category_name')->toarray(0);
            $providerCode = DB::table('tblProviders')->select('abbriviation')->where('provider_id','=',$data['provider'])->first();
           
            unset($data['pickup']);
            unset($data['_token']);
            unset($data['pickupDateTime']);
            unset($data['pickupDes']);
            unset($data['pickuptime']);
            unset($data['pickupDesc']);
            unset($data['note']);
            unset($data['specialNote']);
            unset($data['noteDesc']);
            unset($data['remark']);
            unset($data['standardRemarks']);
            unset($data['remarkDesc']);
            unset($data['is_Addons']);
            unset($data['noAddon']);
            unset($data['random_tour_id']);
            $tripCountries=implode(',', $data['tripCountries']);
            
            $data['tour_url'] = str_replace(array('-','&','.',' ','___','__',"'",','), array('','and','','_','_','_','',''), strtolower($data['tour_title']));
            $data['no_of_days_text'] = $data['no_of_days'];
            $data['tripCountries'] = $tripCountries;
            $data['tripActivities'] = implode(',', $allCategories);
            $data['is_active'] = 1;
            $data['Date_LastUpdate'] = date('Y-m-d H:i:s');
            $data['views'] = 0;
            $data['meta_description'] = $data['short_description'];
            $data['is_approve'] = 0; //1
            $data['is_reviewed'] = 0; //1
            $data['sync_error'] = 'This is new Tour'; //'Tour Updated'
            $data['updated_by'] = 'Admin';
            $data['admin_id_updated'] = -1;
            $data['date_admin_updated'] = date('Y-m-d H:i:s');
            $data['updated_msg'] = 'Tour added by Admin';
            $data['IsLive_PracticalDetail'] = 1;
            $data['tour_type_logo_id']= $data['tour_type_logo_id'];
            $data['is_deleted']= 0;
            if(isset($data['children_age']) && !empty($data['children_age'])){
                    $data['children_age']= $data['children_age'];
            }
            if(isset($data['saving_per_person']) && !empty($data['saving_per_person'])){
                    $data['saving_per_person']= $data['saving_per_person'];
            }else{
                    $data['saving_per_person']= 0;
            }
            if(isset($data['discount']) && !empty($data['discount'])){
                    $data['discount']= $data['discount'];
            }else{
                    $data['discount']= 0;
            }
            $data['from_city_id'] = $data['departure'];
            $data['to_city_id'] = $data['destination'];
            $data['countryData'] = $data['tripCountries'];
            if($oRequest->tour_id == ''){
                $oTour = Tour::create($data);
                $nTourId = $oTour->tour_id;
            }
            else{
                $oTour = Tour::where('tour_id',$oRequest->tour_id)->update($data);
                $nTourId = $oRequest->tour_id;
            }
            
            //$nTourId = $oTour->tour_id;

            $tripActivities = $extraData['tripActivities'];
            foreach ($tripActivities as $key => $value) {
                $activities['tour_id'] = $nTourId;
                $oTourCategory = TourCategory::firstOrNew(['tour_id' => $nTourId]);
                $oTourCategory->category_id = $value;
                //print_r($oTourCategory);exit;
                $oTourCategory->save();
            }
            
            //Add Country
            foreach ($extraData['tripCountries'] as $key => $value) {
                $countries['country_id'] = $value;
                $countries['tour_id'] = $nTourId;
                TourCountry::firstOrNew($countries)->save();
                if($oRequest->tour_id != ''){
                    $aTourCountry = TourCountry::where('tour_id',$nTourId)->pluck('country_id')->toArray();
                    $aDeleteTourCountry = array_diff($aTourCountry, $extraData['tripCountries']);
                    foreach ($aDeleteTourCountry as $value) {
                        TourCountry::where([ 'country_id'=>$value,
                                            'tour_id' =>$oRequest->tour_id                                            
                                            ])->delete();
                    }
                }
            }
            
            $aTourPickup= $aNewTourPickup = array();
            if(array_key_exists('pickuptime', $extraData) && $extraData['pickuptime'] != ''){
                    foreach ($extraData['pickuptime'] as $key => $value) {
                        $createdAt = '';
                        $createdAt = Carbon::parse($value);
                        $createdAt->format('M d Y H:i');
            
                        $oProviderPickup = ProviderPickup::firstOrNew([ 'pickup_time' => $createdAt,
                                                                    'description' => $extraData['pickupDesc'][$key],
                                                                    'provider_id' => $data['provider']]);
                        $oProviderPickup->save();
                        $oTourPickup = TourPickup::firstOrNew([ 'tour_id' => $nTourId,
                                                                'pickup_id' => $oProviderPickup->pickup_id])
                                                ->save();
                        array_push($aNewTourPickup, $oProviderPickup->pickup_id);
                    }
            }

            if(array_key_exists('pickup', $extraData) && $extraData['pickup'] != ''){
                foreach ($extraData['pickup'] as $key => $value) {
                    TourPickup::firstOrNew([ 'pickup_id'=>$value,
                                            'tour_id' =>$nTourId                                            
                                        ])->save();
                }
                //update time for delete unchecked value 
                if($oRequest->tour_id != '')
                {
                    $aPickupAll = array_unique(array_merge ($aNewTourPickup, $extraData['pickup']));
                    $aTourPickup = TourPickup::where(['tour_id' =>$oRequest->tour_id])->pluck('pickup_id')->toArray();
                    $aDeletePickup = array_diff($aTourPickup, $aPickupAll);
                    foreach ($aDeletePickup as $value) {
                        TourPickup::where([ 'pickup_id'=>$value,
                                            'tour_id' =>$oRequest->tour_id                                            
                                            ])->delete();
                    }
                }
            }

            //Add Special Notes
            $aTourSpecialNote= $aNewSpecialNote = array();
            if(array_key_exists('noteDesc', $extraData) && $extraData['noteDesc'] != ''){
                foreach ($extraData['noteDesc'] as $key => $value) {
                    $values = ['special_desc' => $value, 'provider_id' => $data['provider']];	
                    $oProviderSpecialNote = ProviderSpecialNote::firstOrNew($values);
                    $oProviderSpecialNote->save();
                    
                    $TourSpecialNote['special_note_id'] = $oProviderSpecialNote->special_note_id;
                    $TourSpecialNote['tour_id'] = $nTourId;
                    TourSpecialNote::firstOrNew($TourSpecialNote)->save();
                    array_push($aNewSpecialNote, $oProviderSpecialNote->special_note_id);
                    //echo $oProviderSpecialNote->special_note_id.' '.$nTourId;exit;
                }
            }

            if(array_key_exists('note', $extraData) && $extraData['note'] != ''){
                foreach ($extraData['note'] as $key => $value) {
                    $TourSpecialNote['special_note_id'] = $value;
                    $TourSpecialNote['tour_id'] = $nTourId;
                    TourSpecialNote::firstOrNew($TourSpecialNote)->save();
                }
                //update time for delete unchecked value 
                if($oRequest->tour_id != '')
                {
                    $aNotepAll = array_unique(array_merge($aNewSpecialNote, $extraData['note']));
                    $aTourSpecialNote= TourSpecialNote::where(['tour_id' =>$oRequest->tour_id])->pluck('special_note_id')->toArray();
                    $aDeleteNote = array_diff($aTourSpecialNote, $aNotepAll);
                    foreach ($aDeleteNote as $value) {
                        TourSpecialNote::where([ 'special_note_id'=>$value,
                                                'tour_id' =>$oRequest->tour_id                                            
                                                ])->delete();
                    }
                }
            }
        
            //Add Standard Remark
            $aTourStandardRemark= $aNewStandardRemark = array();
            if(array_key_exists('remarkDesc', $extraData) && $extraData['remarkDesc'] != ''){
                foreach ($extraData['remarkDesc'] as $key => $value) {
                    $oProviderStandardRemarks= ProviderStandardRemark::firstOrNew( ['standard_desc' => $value, 'provider_id' => $data['provider']] );
                    $oProviderStandardRemarks->save();
                    $TourStandardRemarks['standard_remarks_id'] = $oProviderStandardRemarks->standard_remarks_id;
                    $TourStandardRemarks['tour_id'] = $nTourId;
                    TourStandardRemark::firstOrNew($TourStandardRemarks)->save();
                    array_push($aNewStandardRemark, $oProviderStandardRemarks->standard_remarks_id);
                }
            }

            if (array_key_exists('remark', $extraData) && $extraData['remark'] != '') {
                foreach ($extraData['remark'] as $key => $value) {
                    $TourStandardRemarks['standard_remarks_id'] = $value;
                    $TourStandardRemarks['tour_id'] = $nTourId;
                    TourStandardRemark::firstOrNew($TourStandardRemarks)->save();
                }
                
                //update time for delete unchecked value 
                if($oRequest->tour_id != '')
                {
                    $aNotepAll = array_unique(array_merge($aNewSpecialNote, $extraData['remark']));
                    $aTourStandardRemark= TourStandardRemark::where(['tour_id' =>$oRequest->tour_id])->pluck('standard_remarks_id')->toArray();
                    $aDeleteRemark = array_diff($aTourStandardRemark, $aNotepAll);
                    foreach ($aDeleteRemark as $value) {
                        TourStandardRemark::where([ 'standard_remarks_id'=>$value,
                                                'tour_id' =>$oRequest->tour_id                                            
                                                ])->delete();
                    }
                }
            }
            
            if(Input::hasFile('mapfile')){
                    $image   = Input::file('mapfile');
                    $image_validator = image_validator($image);
                // check if image is valid
                if( $image_validator['fails'] ){
                    //$error = ['message' => $image_validator['message']];
                }else{
                            
                    $file = $image->getClientOriginalName();
                    $filename = pathinfo($file, PATHINFO_FILENAME);
                    $filename   = $filename.'.'.$image->getClientOriginalExtension();
                    $destination = 'uploads/tours/'.$nTourId;

                    // CREATE ORIGINAL MAP IMAGE
                    $original_map = $destination.'/'.$filename;		    		
                    File::makeDirectory( public_path($destination), 0775, FALSE, TRUE); // create directory if not existing yet
                    $image->move($destination, $filename); 

                    // CREATE MAP IMAGE
                    $map_path = $destination.'/map/'.$filename; // set thumbnail path
                    $map_img  = Image::make($original_map); // create image intervention object from original image
                    $map_img  = resize_image_to_thumbnail( $map_img ); // resize; found in helpers.php
                    File::makeDirectory( public_path($destination).'/map', 0775, FALSE, TRUE);
                    $map_img->save($map_path); // store image 

                    $update['mapfile'] = $filename;
                }
            }
                        
            $update['code'] = $nTourId;
            $update['tour_code'] = $providerCode->abbriviation.' '.$nTourId;
            $tour = Tour::where('tour_id',$nTourId)->update($update);
            if($oRequest->tour_id == '')
            {
                $images = Images::where('random_tour_id','=',$random_tour_id)->get();
                if(!empty($images)){    
                    foreach ($images as $image) {
                        //$random_tour_id = $image->random_tour_id;
                        $image_name = str_replace('/'.$random_tour_id.'/', '/'.$nTourId.'/', $image->image_name);
                        $image_thumb = str_replace('/'.$random_tour_id.'/', '/'.$nTourId.'/', $image->image_thumb);
                        $image_small = str_replace('/'.$random_tour_id.'/', '/'.$nTourId.'/', $image->image_small);
                        $image_medium = str_replace('/'.$random_tour_id.'/', '/'.$nTourId.'/', $image->image_medium);
                        $image_large = str_replace('/'.$random_tour_id.'/', '/'.$nTourId.'/', $image->image_large);
                        $value = ['tour_id' => $nTourId, 'image_name'=>$image_name, 'image_thumb'=>$image_thumb, 'image_small'=>$image_small, 'image_medium'=>$image_medium, 'image_large'=>$image_large];

                        Images::where('image_id','=',$image->image_id)->update( $value );
                    }

                $oldFolder = public_path('uploads/tours/'.$random_tour_id);
                $newFolder = public_path('uploads/tours/'.$nTourId);
                rename($oldFolder,$newFolder);
                }
            }
            Session::flash('message', trans('messages.label_success_msg'));
            return Redirect::back();
        }
        $oCountries = Country::orderBy('name','asc')->pluck('name','id');
        $oRegions = Region::orderBy('name','asc')->pluck('name','id');
        $oProvider = Provider::where('is_active','=',1)->pluck('provider_name','provider_id');
        $TourTypeLogo = TourTypeLogo::pluck('title','id');
        $oCategories  = CategoryDef::orderBy('category_name','Asc')->pluck('category_name','category_id');
        $oCurrencies  = Currency::where('id','=',1)->pluck('code','id');
        
        $oTripCounry =$oTourCategoty =array();
        if($nIdTour != '')
        {
            $oTour = Tour::where('tour_id',$nIdTour)->first();
            $de_countries 	 = City::where('name','=',$oTour->departure)->pluck('country_id');
            $oTour['de_countries'] = (count($de_countries)) ?  $de_countries[0] : '';

            $dn_countries 	 = City::where('name','=',$oTour->destination)->pluck('country_id');
            $oTour['dn_countries'] = (count($dn_countries)) ?  $dn_countries[0] : '';

            $provider_id 	= $oTour->provider; 
            $oProviderPickups = $oProviderNotes = $oProviderRemarks = array();
            if ( $provider_id >= 0) {
                    $oProviderPickups  	= ProviderPickup::where('provider_id','=',$provider_id)->get();
                    $oProviderNotes  	= ProviderSpecialNote::where('provider_id','=',$provider_id)->get();
                    $oProviderRemarks  	= ProviderStandardRemark::where('provider_id','=',$provider_id)->get();
            }

            $oTourPickups  	= TourPickup::where('tour_id','=',$nIdTour)->pluck('pickup_id')->toArray();
            $oTourNotes  	= TourSpecialNote::where('tour_id','=',$nIdTour)->pluck('special_note_id')->toArray();
            $oTourRemarks  	= TourStandardRemark::where('tour_id','=',$nIdTour)->pluck('standard_remarks_id')->toArray();
            $oTourCategoty  	= TourCategory::where('tour_id','=',$nIdTour)->pluck('category_id')->toArray();
            $oTripCounry        = TourCountry::where('tour_id','=',$nIdTour)->pluck('country_id')->toArray();
            $oTourImages 	= Images::where('tour_id','=', $nIdTour)->orderBy('sort_order','asc')->get();
            $oDnCities 	 	= City::orderBy('name','asc')->where('country_id','=', $oTour['dn_countries'])->pluck('name','id');
            $oDeCities 	 	= City::orderBy('name','asc')->where('country_id','=', $oTour['de_countries'])->pluck('name','id');
        }

        return \View::make('WebView::tour.tour_create',compact('oTour','oCountries','oRegions','oProvider','TourTypeLogo','oCategories','oCurrencies',
                                                                'oDnCities','oDeCities','oTourImages','oTourCategoty','oTourRemarks','oTourNotes',
                                                                'oTourPickups','oProviderRemarks','oProviderNotes','oProviderPickups','oTripCounry','nIdTour'));
    }
    
    public function getProviderData(Request $oRequest)
    {
        $provider_id = $oRequest->provider_id; 
        if ($provider_id >= 0) {

            $pickupHtml = '<div class="row"><div class="col-sm-1"><label class="supplier_check radio-checkbox label_check " for="checkbox-suppliers"><input type="checkbox" class="checkbox-suppliers" id="checkbox-suppliers" value="1">&nbsp;</label></div><div class="col-sm-3">Pickup Time</div><div class="col-sm-8">Description</div></div><hr>';
            $noteHtml = '<div class="row"><div class="col-sm-1"><label class="note_check radio-checkbox label_check" for="checkbox-notes"><input type="checkbox" class="checkbox-notes" id="checkbox-notes" value="1">&nbsp;</label></div><div class="col-sm-11">Description</div></div><hr>';
            $remarkHtml = '<div class="row"><div class="col-sm-1"><label class="remark_check radio-checkbox label_check" for="checkbox-remarks"><input type="checkbox" class="checkbox-remarks" id="checkbox-remarks" value="1">&nbsp;</label></div><div class="col-sm-11">Description</div></div><hr>';

            $pickups = DB::table('tblProviderPickups')->where('provider_id', '=', $provider_id)->get();
            $notes = DB::table('tblProviderSpecialNotes')->where('provider_id', '=', $provider_id)->get();
            $remarks = DB::table('tblProviderStandardRemarks')->where('provider_id', '=', $provider_id)->get();
            //echo '<pre>'; print_r($remarks); 

            if ($pickups) {
                foreach ($pickups as $pickup) {
                    $pickupHtml .= '<div class="row"><div class="col-sm-1"><label class="radio-checkbox label_check" for="checkbox-pickup-' . $pickup->pickup_id . '"><input type="checkbox" class="cmp_supplier_check" name="pickup[]" id="checkbox-pickup-' . $pickup->pickup_id . '" value="' . $pickup->pickup_id . '">&nbsp;</label></div><div class="col-sm-3">' . $pickup->pickup_time . '</div><div class="col-sm-8">' . $pickup->description . '</div></div>';
                }
            } else {
                $pickupHtml = '<div class="error">There is no pickup location added against this Supplier.</div>';
            }

            if ($notes) {
                foreach ($notes as $note) {
                    $noteHtml .= '<div class="row"><div class="col-sm-1"><label class="radio-checkbox label_check" for="checkbox-note-' . $note->special_note_id . '"><input type="checkbox" class="cmp_note_check" name="note[]" id="checkbox-note-' . $note->special_note_id . '" value="' . $note->special_note_id . '">&nbsp;</label></div><div class="col-sm-8">' . $note->special_desc . '</div></div>';
                }
            } else {
                $noteHtml = '<div class="error">There is no special note added against this Supplier.</div>';
            }

            if ($remarks) {
                foreach ($remarks as $remark) {
                    $remarkHtml .= '<div class="row"><div class="col-sm-1"><label class="radio-checkbox label_check" for="checkbox-remark-' . $remark->standard_remarks_id . '"><input type="checkbox" class="cmp_remark_check" name="remark[]" id="checkbox-remark-' . $remark->standard_remarks_id . '" value="' . $remark->standard_remarks_id . '">&nbsp;</label></div><div class="col-sm-8">' . $remark->standard_desc . '</div></div>';
                }
            } else {
                $remarkHtml = '<div class="error">There is no standard remark added against this Supplier.</div>';
            }

            return array($pickupHtml, $noteHtml, $remarkHtml);
        } else {
            return null;
        }
    }
    
    public function ImageUpload(Request $oRequest) 
    {
        $success = FALSE;
        $data = array();
        $error = array();
        // determine if a file has been uploaded

        if (Input::hasFile('tourImage')) 
        {
            $nTourId = Input::get('tour_id');
            $image = Input::file('tourImage')[0];
            
            $oRequest->offsetSet('extension',$image->getClientOriginalExtension());
            $oValidator = Validator::make($oRequest->all(), [
                                        'tourImage' => 'required',
                                        'extension' => 'required|in:jpg,jpeg,png'
                                        ]);
            if($oValidator->fails()) 
            {
                return response()->json(['success' => $oValidator->errors()],422);
            }
            
            $image_validator = image_validator($image);
            // check if image is valid
            if ($image_validator['fails']) {
                $error = ['message' => $image_validator['message']];
            } else {
                if (!is_numeric($nTourId)) {
                    $nTourId = substr($nTourId, 5);
                    $countName = DB::table('tblImages')->where(['title' => $image->getClientOriginalName(), 'random_tour_id' => $nTourId])->count();  // if rand tour id 
                    $values = array('random_tour_id' => $nTourId, 'image_name' => $image->getClientOriginalName());
                    if ($countName >= 1) {
                        $success = false;
                        $data = $values;
                        $error = ['success' => false];
                        return response_format($success, $data, $error);
                    }
                    $count = DB::table('tblImages')->where(['random_tour_id' => $nTourId])->count();  // if rand tour id
                    $values = array('random_tour_id' => $nTourId);
                } else {
                    $countName = DB::table('tblImages')->where(['title' => $image->getClientOriginalName(), 'tour_id' => $nTourId])->count();  // if rand tour id 
                    $values = array('tour_id' => $nTourId, 'image_name' => $image->getClientOriginalName());
                    if ($countName >= 1) {
                        $success = false;
                        $data = $values;
                        $error = ['success' => false];
                        return response_format($success, $data, $error);
                    }
                    $count = DB::table('tblImages')->where(['tour_id' => $nTourId])->count();
                    $values = array('tour_id' => $nTourId);
                }

                $file = $image->getClientOriginalName();
                $filename = pathinfo($file, PATHINFO_FILENAME);
                $filename = $filename . '.' . $image->getClientOriginalExtension();
                // the path where the image is saved
                $destination = 'uploads/tours/' . $nTourId;

                // CREATE ORIGINAL IMAGE
                $original_path = $destination . '/' . $filename;
                File::makeDirectory(public_path($destination), 0775, FALSE, TRUE); // create directory if not existing yet
                $image->move($destination, $filename);
                $values = array_merge($values, ['image_name' => $original_path]);

                // CREATE THUMBNAIL IMAGE
                $thumbnail_path = $destination . '/thumbnail/' . $filename; // set thumbnail path
                $thumbnail_img = Image::make($original_path); // create image intervention object from original image
                $thumbnail_img = resize_image_for_tour($thumbnail_img); // resize; found in helpers.php
                File::makeDirectory(public_path($destination) . '/thumbnail', 0775, FALSE, TRUE);
                $thumbnail_img->save($thumbnail_path); // store image 
                $values = array_merge($values, ['image_thumb' => $thumbnail_path]); // thumbnail path to be stored in db
                // CREATE SMALL IMAGE
                $small_path = $destination . '/small/' . $filename; // set small path
                $small_img = Image::make($original_path); // create image intervention object from original image
                $small_img = resize_image_to_small($small_img); // resize; found in helpers.php
                File::makeDirectory(public_path($destination) . '/small', 0775, FALSE, TRUE);
                $small_img->save($small_path); // store image 
                $values = array_merge($values, ['image_small' => $small_path]); // small path to be stored in db		
                // CREATE MEDIUM IMAGE
                $medium_path = $destination . '/medium/' . $filename; // set medium path
                $medium_img = Image::make($original_path); // create image intervention object from original image
                $medium_img = resize_image_to_medium($medium_img); // resize; found in helpers.php
                File::makeDirectory(public_path($destination) . '/medium', 0775, FALSE, TRUE);
                $medium_img->save($medium_path); // store image 
                $values = array_merge($values, ['image_medium' => $medium_path]); // medium path to be stored in db	
                // CREATE LARGE IMAGE
                $large_path = $destination . '/large/' . $filename; // set large path
                $large_img = Image::make($original_path); // create image intervention object from original image
                $large_img = resize_image_to_large($large_img); // resize; found in helpers.php
                File::makeDirectory(public_path($destination) . '/large', 0775, FALSE, TRUE);
                $large_img->save($large_path); // store image 
                $values = array_merge($values, ['image_large' => $large_path]); // large path to be stored in db

                $sort_order = DB::table('tblImages')->where('tour_id', '=', $nTourId)->max('sort_order');
                $values = array_merge($values, ['is_active' => 1, 'title' => $filename, 'sort_order' => $sort_order + 1, 'is_primary' => 0]);

                // STORE TO DATABASE
                $id = DB::table('tblImages')->insertGetId($values);

                $values = array_merge($values, ['image_id' => $id]);

                if ($id) {
                    $success = TRUE;
                    $data = $values; //->toArray();
                    $data['image_name'] = $filename;
                    $data = ['success' => true, $data];
                } else {
                    $error = ['success' => false];
                }
            }
        } else {
            $error = ['success' => false];
        }
        return response_format($success, $data, $error);
    }
    
    public function callTourImageDelete($nIdTourImage) 
    {  
        $data    = array();
        $success = TRUE;
        $oImage = Images::find($nIdTourImage);
        if( $oImage )
        {
            $error = array();
            File::delete( public_path( $oImage->thumbnail ) );
            File::delete( public_path( $oImage->small ) );
            File::delete( public_path( $oImage->medium ) );
            File::delete( public_path( $oImage->large ) );
            File::delete( public_path( $oImage->original ) );
            $delete = $oImage->delete();
            if(!$delete){
                    $success = FALSE;
                    $error = ['message' => 'An error has occured while trying to delete the image.'];
            }else
            {
                $data = ['message' => 'Successfully deleted image.'];
                if($oImage->is_primary == TRUE){
                    $count = Images::where(['tour_id' => $oImage->tour_id])->orWhere('random_tour_id',$oImage->random_tour_id)->count();
                    if( $count > 1){
                        $new_primary_image = Images::where(['tour_id' => $oImage->tour_id])->orWhere('random_tour_id',$oImage->random_tour_id)->first();
                        Images::where(['image_id' => $new_primary_image->image_id])->update(['is_primary' => 1]);
                    }	
                }
            }
        }else{
                $error = ['message' => 'An error has occured. Image not found.'];
        }
        return response_format($success, $data, $error);
    }
    
    public function sortImages()
    {
        //echo "dsfd";exit;
        $data = Input::only('image_id');
        if($data){
            foreach ($data['image_id'] as $key => $value) {
                if($key == 0){
                        $primary = 1;
                }else{
                        $primary = 0;
                }
                $sort_order = $key + 1;
                Images::where(['image_id' => $value])->update(['is_primary' => $primary,'sort_order'=>$sort_order]);
            }
        }
        return "success";
    }
    
    public function callChangeStatus(){
        $aTours = Input::get('tours');
        $sAction = Input::get('action');

        DB::enableQueryLog();
        
        switch ( $sAction ) {
                case 'delete':
                        if($aTours){
                            $update_data = array('is_deleted'=>1, 'delete_by'=>'Admin', 'id_delete_by'=>-1);
                            Tour::whereIn('tour_id', $aTours)->update($update_data);
                        }
                        $msg = 'Tour Deleted Successfully.';
                        break;
                case 'mark_as_unreviewed':
                        if($aTours){
                            $update_data = array('is_reviewed'=>0, 'is_active'=>0);
                            Tour::whereIn('tour_id', $aTours)->update($update_data);
                        }
                        $msg = 'Tour Un-Reviewed and De-activated Successfully.';
                        break;
                case 'mark_as_reviewed':
                        if($aTours){
                            $update_data = array('is_reviewed'=>1, 'is_active'=>1);
                            Tour::whereIn('tour_id', $aTours)->update($update_data);
                        }
                        $msg = 'Tour Reviewed and activated Successfully.';
                        break;
                case 'activate':
                        if($aTours){
                            $update_data = array('is_active'=>1);
                            Tour::whereIn('tour_id', $aTours)->update($update_data);
                        }
                        $msg = 'Tour Activated Successfully.';
                        break;
                case 'deactivate':
                        if($aTours){
                                foreach ($aTours as $tour) {
                                        $update_data = array('is_active'=>0);
                                Tour::where('tour_id', $tour)->update($update_data);
                                }
                        }
                        $msg = 'Tour De-activated Successfully.';
                        break;
                default:
                        break;
        }
        Session::flash('message', $msg);
        return 'success';
    }
    
    public function callManageView(){
        $sAction = Input::get('action');
        $tour 	= Input::get('tours'); 
        $views 	= Input::get('views'); 

        if($sAction == 'Update'){
            $update_data = array('views'=>$views);
            Tour::where('tour_id', $tour)->update($update_data);	
        }
        return 1;
    }
    
    public function callManageTourDates(Request $oRequest,$nTourId) {
        DB::enableQueryLog();
        $oTour = Tour::where('tour_id', $nTourId)->with('currency')->first()->toArray();
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : NULL;
        $oDates = Season::getSeasonList($nTourId,$sSearchBy);
       //print_r($oDates);exit;
        $seasonDates = [];
        if ($oDates) {
            foreach ($oDates as $date) {

                $sDates = DB::table('tblDates')
                        ->where('tour_id', '=', $nTourId)
                        ->where('season_id', '=', $date->season_id)
                        ->get();
                $seasonDates[$date->season_id] = $sDates;
            }
        }

        $oPayments = $this->localPaymentList($nTourId);
        $oCurrencies = Currency::where('id', '=', 1)->pluck('code', 'id');
        DB::table('tblLocalPayment')->where('tour_id', '=', $nTourId)->where('isMandatory', '=', 0)->delete();
        DB::table('tblFlightPayment')->where('tour_id', '=', $nTourId)->where('isMandatory', '=', 0)->delete();
        $oLocalPayment = DB::table('tblLocalPayment')->where('tour_id', '=', $nTourId)->get();
        $oFlightPayment = DB::table('tblFlightPayment')->where('tour_id', '=', $nTourId)->get();
        return \View::make('WebView::tour.manage_dates',compact('oLocalPayment', 'oFlightPayment', 'oTour', 'oDates', 'oCurrencies', 'oPayments', 'seasonDates'));
    }

    private function localPaymentList($nTourid)
    {
        $payments = LocalPayment::getLocalPayment($nTourid);
        $localList = [];
        if($payments){
            $i=0;
            foreach ($payments as $payment) {
                $localList[$payment->season_id][] = '<div class="row">
                                                        <div class="col-sm-2">Local payment</div>
                                                        <div class="col-sm-2">'.$payment->code.' '.$payment->Price.'</div>
                                                        <div class="col-sm-2">'.$payment->description.'</div>
                                                        <div class="col-sm-6"></div>
                                                    </div>';	
                $i++;
            }
        }
        return $localList;
    }
    
    public function callRemoveSeason(){
        $nTourId 	= Input::get('tourId');
        $nSeasonId	= Input::get('season_id');
        if(!empty($nSeasonId) && !empty($nTourId)){
                DB::table('tblLocalPayment')->where('season_id', '=', $nSeasonId)->delete();
                DB::table('tblFlightPayment')->where('season_id', '=', $nSeasonId)->delete();
                DB::table('tblSeason')->whereIn('season_id',$nSeasonId)->delete();
                DB::table('tblDates')->where('tour_id', '=', $nTourId)->where('season_id', '=', $nSeasonId)->delete();
                echo '1';
        }else{
                echo '2';
        }
        exit;
    }
    
    public function callManageFlightPayment() {
        $sAction = Input::get('action');
        $nTourId = Input::get('tourId');
        $Mandatory = 0;

        if ($sAction == 'Add') {
            $nSeasonId = Input::get('season_id');

            if ($nSeasonId > 0) {
                $Mandatory = 1;
            }
            $aFlightPaymentData = ['tour_id' => $nTourId,
                                    'flightPrice' => Input::get('flightPrice'),
                                    'flightDescription' => Input::get('flightDescription'),
                                    'flight_depart_city' => Input::get('flight_depart_city'),
                                    'flight_currency_id' => Input::get('flight_currency_id'),
                                    'season_id' => Input::get('season_id'),
                                    'isMandatory' => $Mandatory];
            FlightPayment::create($aFlightPaymentData);
        } elseif ($sAction == 'Delete') {
            $nSeasonId = Input::get('season_id');
            $nFlightId = Input::get('flight_id');
            DB::table('tblFlightPayment')->where('flight_id', '=', $nFlightId)->delete();
        } else {
            $nSeasonId = Input::get('season_id');
        }

        $string = '';
        if ($nSeasonId == 0) {
            $oDates = DB::table('tblFlightPayment')
                    ->select('tblFlightPayment.*', 'zCurrencies.code', 'zCities.name')
                    ->where('tour_id', '=', $nTourId)
                    ->where('isMandatory', '=', 0)
                    ->leftJoin('zCurrencies', 'tblFlightPayment.flight_currency_id', '=', 'zCurrencies.id')
                    ->leftJoin('zCities', 'tblFlightPayment.flight_depart_city', '=', 'zCities.id')
                    ->get();
        } else {
            $oDates = FlightPayment::select('tblFlightPayment.*', 'zCurrencies.code', 'zCities.name')
                                ->where('tour_id', '=', $nTourId)
                                ->where('season_id', '=', $nSeasonId)
                                ->leftJoin('zCurrencies', 'tblFlightPayment.flight_currency_id', '=', 'zCurrencies.id')
                                ->leftJoin('zCities', 'tblFlightPayment.flight_depart_city', '=', 'zCities.id')
                                ->orderBy('flight_id', 'Asc')
                                ->get();
        }

        if ($oDates) {
            $i = 0;
            foreach ($oDates as $date) {
                $string .= '<div class="row totalFlight m-t-10">
                                <div class="col-sm-2"><div class="form-group">
                                    <label class="label-control"></label>
                                        <input value="' . $date->code . '" class="form-control" type="text" disabled>
                                        <input name="flight_id[]" class="flight_id" value="' . $date->flight_id . '" type="hidden">
                                </div></div>
                                <div class="col-sm-2"><div class="form-group">
                                    <label class="label-control"></label>
                                    <input class="form-control" value="' . $date->flightDescription . '" type="text" disabled>
                                </div></div>
                                <div class="col-sm-2"><div class="form-group">
                                    <label class="label-control"></label>
                                    <input value="' . $date->name . '" class="form-control" type="text" disabled>
                                </div></div>
                                <div class="col-sm-2"><div class="form-group">
                                    <label class="label-control"></label>
                                    <input class="form-control" value="' . $date->flightPrice . '" type="text" disabled>
                                </div></div>
                                <div class="col-sm-2"><div style="margin-bottom:0px;">
                                    <input type="button" class="btn btn-primary btn-block localFightPaymentDelete" id="localFightPaymentDelete_' . $i . '"  data-id="' . $date->flight_id . '" data-currency="' . $date->flight_depart_city . '" value="Remove">
                                </div></div>
                            </div>';
                $i++;
            }
            
        }
        return $string;
    }
    
    public function callManageSeason(){	
        $sAction = Input::get('action');
        $nTourId = Input::get('tourId');
        $nSeasonId = Input::get('season_id');


        if ($sAction == 'Add') {
            if ($nSeasonId > 0) {
                $update_data = array(
                    'AdultPrice' => Input::get('AdultPrice'),
                    'Availability' => Input::get('Availability'),
                    'SeasonName' => Input::get('SeasonName'),
                    'AdultPriceSingle' => Input::get('AdultPriceSingle'),
                    'AdultSupplement' => Input::get('AdultPriceSupplement'),
                    'sumPrice' => Input::get('AdultPrice')
                );
                DB::table('tblSeason')->where('season_id', $nSeasonId)->update($update_data);

                $updateDates = array(
                    'AdultPrice' => Input::get('AdultPrice'),
                    'Availability' => Input::get('Availability'),
                    'AdultPriceSingle' => Input::get('AdultPriceSingle'),
                    'AdultSupplement' => Input::get('AdultPriceSupplement')
                );
                DB::table('tblDates')->where('tour_id', $nTourId)->where('season_id', $nSeasonId)->update($updateDates);
            } else {
                $insertArray = array('tour_id' => $nTourId,
                    'StartDate' => date('Y-m-d', strtotime(Input::get('StartDate'))),
                    'EndDate' => date('Y-m-d', strtotime(Input::get('EndDate'))),
                    'AdultPrice' => Input::get('AdultPrice'),
                    'Availability' => Input::get('Availability'),
                    'adv_purchase' => 0,
                    'is_mon' => 0,
                    'is_tue' => 0,
                    'is_wed' => 0,
                    'is_thu' => 0,
                    'is_fri' => 0,
                    'is_sat' => 0,
                    'is_sun' => 0,
                    'SeasonName' => Input::get('SeasonName'),
                    'AdultPriceSingle' => Input::get('AdultPriceSingle'),
                    'AdultSupplement' => Input::get('AdultPriceSupplement'),
                    'sumPrice' => Input::get('AdultPrice')
                );
                $season_id = DB::table('tblSeason')->insertGetId($insertArray);
                DB::table('tblLocalPayment')->whereIn('Payment_Id', Input::get('PaymentId'))->where('tour_id', '=', $nTourId)->update(array('season_id' => $season_id, 'isMandatory' => 1));
                DB::table('tblFlightPayment')->whereIn('flight_id', Input::get('flight_id'))->where('tour_id', '=', $nTourId)->update(array('season_id' => $season_id, 'isMandatory' => 1));
                $update['tour_currency'] = Input::get('currency');
                $tour = Tour::where('tour_id', $nTourId)->update($update);
            }
        } elseif ($sAction == 'Delete') {
            DB::table('tblLocalPayment')->where('season_id', '=', $season_id)->delete();
            DB::table('tblFlightPayment')->where('season_id', '=', $season_id)->delete();
            DB::table('tblSeason')->where('season_id', '=', $season_id)->delete();
            DB::table('tblDates')->where('tour_id', '=', $nTourId)->where('season_id', '=', $season_id)->delete();
        }

        //UPDATE TOUR PRICE
        $currentdate = date('Y-m-d');
        $prices = DB::table('tblSeason')->select('StartDate', 'EndDate', 'AdultPrice', 'ChildPrice', 'AdultPriceSingle', 'AdultSupplement', 'ChildPriceSingle', 'ChildSupplement', 'flight_currency_id', 'flightPrice', 'flightDescription', 'flightReturn', 'flightDepart')
                ->where('EndDate', '>=', $currentdate)
                ->where('tour_id', $nTourId)
                ->where('flightPrice', '>', 0)
                ->orderBy('sumPrice', 'ASC')
                ->first();
        if (empty($prices)) {
            $prices = DB::table('tblSeason')->select('StartDate', 'EndDate', 'AdultPrice', 'ChildPrice', 'AdultPriceSingle', 'AdultSupplement', 'ChildPriceSingle', 'ChildSupplement', 'flight_currency_id', 'flightPrice', 'flightDescription', 'flightReturn', 'flightDepart')
                    ->where('EndDate', '>=', $currentdate)
                    ->where('tour_id', $nTourId)
                    ->orderBy('AdultPrice', 'ASC')
                    ->first();
        }

        if ($prices) {
            $tourUpdate = array(
                'price' => $prices->AdultPrice,
                'start_date' => $prices->StartDate,
                'end_date' => $prices->EndDate,
                'ChildPrice' => $prices->ChildPrice,
                'AdultPriceSingle' => $prices->AdultPriceSingle,
                'AdultSupplement' => $prices->AdultSupplement,
                'ChildPriceSingle' => $prices->ChildPriceSingle,
                'ChildSupplement' => $prices->ChildSupplement,
                'flight_currency_id' => $prices->flight_currency_id,
                'flightPrice' => $prices->flightPrice,
                'flightDescription' => $prices->flightDescription,
                'flightReturn' => $prices->flightReturn,
                'flightDepart' => $prices->flightDepart
            );
            DB::table('tblTours')->where('tour_id', $nTourId)->update($tourUpdate);
        }

        $string = '';
        $dates = DB::table('tblSeason')
                ->where('tblSeason.tour_id', '=', $nTourId)
                ->get();

        $seasonDates = [];
        if ($dates) {
            foreach ($dates as $date) {
                $sDates = DB::table('tblDates')
                        ->where('tour_id', '=', $nTourId)
                        ->where('season_id', '=', $date->season_id)
                        ->get();
                $seasonDates[$date->season_id] = $sDates;
            }
        }

        if ($dates) {
            $payments = $this->localPaymentList($nTourId);
            $todaydate = date('Y-m-d');
            $i = 0;
            foreach ($dates as $date) {

                $StartDate = Carbon::createFromFormat('Y-m-d H:i:s',$date->StartDate);
                $EndDate = Carbon::createFromFormat('Y-m-d H:i:s',$date->EndDate);

                $disDays = $EndDate->diffInDays($StartDate);
                //$disDays = $diff->format("%R%a");

                $visDays = $StartDate->diffInDays($EndDate);
                //$visDays = $diff2->format("%R%a");
                
                $diffMonth = $StartDate->diffInMonths($EndDate);

                $string .= '<tr>
                                <th id="seasonname_' . $i . '">' . $date->SeasonName . '</th>
                                <th>' . date('d-m-Y', strtotime($date->StartDate)) . '<span id="startDate_' . $i . '" style="display:none;">' . date('Y-m-d', strtotime($date->StartDate)) . '</span></th>
                                <th>' . date('d-m-Y', strtotime($date->EndDate)) . '<span id="endDate_' . $i . '" style="display:none;">' . date('Y-m-d', strtotime($date->EndDate)) . '</span></th>
                                <th>' . number_format($date->AdultPrice, 2) . '</th>
                                <th id="availability_' . $i . '">' . $date->Availability . '</th>
                                <th>
                                    <button type="button" class="btn btn-primary dateRangeEdit" data-id= "' . $i . '" id="dateRangeEdit_' . $i . '" title="Edit">Edit</button>
                                    <button type="button" class="btn btn-primary dateRangeDelete" data-id= "' . $date->season_id . '" id="dateRangeDelete_' . $i . '" title="Delete">Delete</button>
                                    <button type="button" class="btn btn-primary dateRangeUpdate" data-advPur="' . $date->adv_purchase . '" data-id= "' . $i . '" data-day = "' . $visDays . '" data-month = "' . $diffMonth . '" id="dateRangeUpdate_' . $i . '" title="Edit">Dates</button>

                                    <input type="hidden" id="adultPriceSingle_' . $i . '" value="' . $date->AdultPriceSingle . '" >
                                    <input type="hidden" id="childPriceSingle_' . $i . '" value="' . $date->ChildPriceSingle . '" >
                                    <input type="hidden" id="adultPrice_' . $i . '" value="' . $date->AdultPrice . '" >
                                    <input type="hidden" id="childPrice_' . $i . '" value="' . $date->ChildPrice . '" >
                                    <input type="hidden" id="adultSupplement_' . $i . '" value="' . $date->AdultSupplement . '" >
                                    <input type="hidden" id="childSupplement_' . $i . '" value="' . $date->ChildSupplement . '" >

                                    <input type="hidden" name="season_id" id="season_id_' . $i . '" value="' . $date->season_id . '" >
                                    <input type="hidden" id="sDateSDAll_' . $i . '" value = "' . count($seasonDates[$date->season_id]) . '" >
                                    <input type="hidden" name="tour_id" id="tour_id" value="' . $date->tour_id . '" >
                                    <input type="hidden" name="adv_purchase" id="adv_purchase_' . $i . '" value="' . $date->adv_purchase . '" >
                                    <input type="hidden" name="is_week" id="is_week_purchase" data-mon="' . $date->is_mon . '" data-tue="' . $date->is_tue . '" data-wed="' . $date->is_wed . '" data-thu="' . $date->is_thu . '" data-fri="' . $date->is_fri . '" data-sat="' . $date->is_sat . '" data-sun="' . $date->is_sun . '" >
                                </th>
                            </tr>';  //<th>'.number_format($date->ChildPrice,2).'</th>
                if (array_key_exists($date->season_id, $payments) && is_array($payments[$date->season_id])) {
                    $string .= '<tr><td colspan="7" style="padding: 0px 10px;border-top: 0;color: #b2b2b2;">';
                    foreach ($payments[$date->season_id] as $payment) {
                        $string .= $payment;
                    }
                    $string .= '</td> </tr>';
                }
                $i++;
            }
        }
        return $string;
    }
    
    public function callManagePayment(){
        $sAction = Input::get('action');
        $nTourId = Input::get('tourId');
        $Mandatory = 0;

        if ($sAction == 'Add') {
            $nSeasonId = Input::get('season_id');

            if ($nSeasonId > 0) {
                $Mandatory = 1;
            }
            $aPaymentData = ['tour_id' => $nTourId,
                            'Price' => Input::get('price'),
                            'description' => Input::get('description'),
                            'currency' => Input::get('currency'),
                            'season_id' => Input::get('season_id'),
                            'isMandatory' => $Mandatory];
            LocalPayment::create($aPaymentData);
        } elseif ($sAction == 'Delete') {
            $nSeasonId = Input::get('season_id');
            $Payment_Id = Input::get('Payment_Id');
            LocalPayment::where('Payment_Id', '=', $Payment_Id)->delete();
        } else {
            $nSeasonId = Input::get('season_id');
        }

        $string = '';
        if ($nSeasonId == 0) {
            $oDates = LocalPayment::where('tour_id', '=', $nTourId)
                    ->where('isMandatory', '=', 0)
                    ->join('zCurrencies', 'tblLocalPayment.currency', '=', 'zCurrencies.id', 'LEFT')
                    ->get();
        } else {
            $oDates = LocalPayment::where('tour_id', '=', $nTourId)
                    ->where('season_id', '=', $nSeasonId)
                    ->join('zCurrencies', 'tblLocalPayment.currency', '=', 'zCurrencies.id', 'LEFT')
                    ->orderBy('Payment_Id', 'Asc')
                    ->get();
        }

        if ($oDates) {
            $i = 0;
            foreach ($oDates as $date) {
                $string .= '<div class="row">
                                <div class="col-sm-3"><div class="form-group">
                                    <label class="label-control"></label>
                                    <input value="' . $date->code . '" class="form-control" type="text" disabled>
                                    <input name="PaymentId[]" class="PaymentId" value="' . $date->Payment_Id . '" type="hidden">
                                </div></div>
                                <div class="col-sm-3"><div class="form-group">
                                    <label class="label-control"></label>
                                    <input value="' . $date->Price . '" class="form-control" type="text" disabled>
                                </div></div>
                                <div class="col-sm-3"><div class="form-group">
                                    <label class="label-control"></label>
                                    <input class="form-control" value="' . $date->description . '" type="text" disabled>
                                </div></div>
                                <div class="col-sm-3"><div style="margin-bottom:0px;">
                                    <input type="button" class="btn btn-primary btn-block localPaymentDelete" id="localPaymentDelete_' . $i . '"  data-id="' . $date->Payment_Id . '" value="Remove">
                                </div></div>
                            </div>';
                $i++;
            }
            
        }
        return $string;
    }

    public function callUpdateSeasonDates($nTourId){
        $AdultPrice = Input::get('AdultPrice');
        $AdultPriceSingle = Input::get('AdultPriceSingle');
        $AdultSupplement = Input::get('AdultSupplement');
        $Availability = Input::get('Availability');
        $adv_purchase = Input::get('adv_purchase');
        $season_id = Input::get('season_id');
        $dates = Input::get('dates');
        $mon = Input::get('mon');
        $tue = Input::get('tue');
        $wed = Input::get('wed');
        $thu = Input::get('thu');
        $fri = Input::get('fri');
        $sat = Input::get('sat');
        $sun = Input::get('sund');
        if (Input::get('tourDuration') > 0) {
            $tourDuration = Input::get('tourDuration') - 1;
        } else {
            $tourDuration = Input::get('tourDuration');
        }


        //UPDATE SEASON
        $seasonArray = array(
            'adv_purchase' => $adv_purchase,
            'is_mon' => $mon,
            'is_tue' => $tue,
            'is_wed' => $wed,
            'is_thu' => $thu,
            'is_fri' => $fri,
            'is_sat' => $sat,
            'is_sun' => $sun
        );
        Season::where('tour_id', $nTourId)->where('season_id', $season_id)->update($seasonArray);

        //DELETE SEASON DATES
        Dates::where('tour_id', $nTourId)->where('season_id', $season_id)->delete();

        //INSERT SEASON DATES
        $i = 0;
        if ($dates) {
            foreach ($dates as $key => $value) {
                $EndDate = date('Y-m-d', strtotime($value . '+' . $tourDuration . ' days'));

                $dateArray = array(
                    'tour_id' => $nTourId,
                    'season_id' => $season_id,
                    'StartDate' => $value,
                    'EndDate' => $EndDate,
                    'AdultPrice' => $AdultPrice, 
                    'Availability' => $Availability,
                    'adv_purchase' => $adv_purchase,
                    'AdultPriceSingle' => $AdultPriceSingle,
                    'AdultSupplement' => $AdultSupplement,
                );
                Dates::create($dateArray);
                $i++;
            }
        }

        //UPDATE TOUR PRICE
        $currentdate = date('Y-m-d');
        $prices = Season::select('StartDate', 'EndDate', 'AdultPrice', 'ChildPrice', 'AdultPriceSingle', 'AdultSupplement', 'ChildPriceSingle', 'ChildSupplement', 'flight_currency_id', 'flightPrice', 'flightDescription', 'flightReturn', 'flightDepart')
                ->where('EndDate', '>=', $currentdate)
                ->where('tour_id', $nTourId)
                ->where('flightPrice', '>', 0)
                ->orderBy('sumPrice', 'ASC')
                ->first();
        if (empty($prices)) {
            $prices = Season::select('StartDate', 'EndDate', 'AdultPrice', 'ChildPrice', 'AdultPriceSingle', 'AdultSupplement', 'ChildPriceSingle', 'ChildSupplement', 'flight_currency_id', 'flightPrice', 'flightDescription', 'flightReturn', 'flightDepart')
                    ->where('EndDate', '>=', $currentdate)
                    ->where('tour_id', $nTourId)
                    ->orderBy('AdultPrice', 'ASC')
                    ->first();
        }
        if ($prices) {
            $tourUpdate = array(
                'price' => $prices->AdultPrice,
                'start_date' => $prices->StartDate,
                'end_date' => $prices->EndDate,
                'ChildPrice' => $prices->ChildPrice,
                'AdultPriceSingle' => $prices->AdultPriceSingle,
                'AdultSupplement' => $prices->AdultSupplement,
                'ChildPriceSingle' => $prices->ChildPriceSingle,
                'ChildSupplement' => $prices->ChildSupplement,
                'flight_currency_id' => $prices->flight_currency_id,
                'flightPrice' => $prices->flightPrice,
                'flightDescription' => $prices->flightDescription,
                'flightReturn' => $prices->flightReturn,
                'flightDepart' => $prices->flightDepart
            );
            Tour::where('tour_id', $nTourId)->update($tourUpdate);
        }

        Session::flash('message', $i . " Dates Saved Successfully");
        return $i;
    }

    public function callTourTypeLogoList(Request $oRequest)
    {
        session(['page_name' => 'tourtypelogo']);
        
        //remove session when it comes from sidebar
        if(session('page_name') != 'tourtypelogo')
            $oRequest->session()->forget('tourlogo');

        session(['page_name' => 'tourlist']);
        $aData = session('tourlogo') ? session('tourlogo') : array();

        $oRequest->session()->forget('tourlogo');
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        
//        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : NULL;
//        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : 'id';
//        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : 'desc';
        $oTourTypeLogoList = TourTypeLogo::getTourTypeLogoList($sSearchStr,$sOrderField,$sOrderBy);
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,'','','tourlogo');
        
        $oViewName = $oRequest->isMethod('GET') ? 'WebView::tour.tour_logo_list' : 'WebView::tour._more_tour_logo_list';
        return \View::make($oViewName, compact('oTourTypeLogoList','sSearchStr','sOrderField','sOrderBy'));
    }
    
    public function callTourTypeLogoCreate(Request $oRequest,$nIdTourLogo = '') 
    {
        session(['page_name' => 'tourtypelogo']);
        if($oRequest->isMethod('post'))
        {
            $data = Input::only('title');
            $rules = array(
                'title' => 'required'
            );

            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            } else {
                $TourTypeLogos = TourTypeLogo::firstOrNew(['id'=>$oRequest->id_logo]);
                $TourTypeLogos->title = $oRequest->title;
                $TourTypeLogos->save();
                $TourTypeLogo_id = $TourTypeLogos->id;
                //Upload logo
                if (Input::hasFile('logo')) {
                    $image = Input::file('logo');
                    $file = $image->getClientOriginalName();
                    $filename = pathinfo($file, PATHINFO_FILENAME);
                    $filename = $filename . '.' . $image->getClientOriginalExtension();
                    // the path where the image is saved
                    $destination = 'uploads/TourType/' . $TourTypeLogo_id;

                    // CREATE ORIGINAL IMAGE
                    $original_path = $destination . '/' . $filename;
                    File::makeDirectory(public_path($destination), 0775, FALSE, TRUE); // create directory if not existing yet
                    $image->move($destination, $filename);

                    // CREATE 37-51 IMAGE
                    $path = $destination . '/37-51/' . $filename; // set 37-51 path
                    $img = Image::make($original_path); // create image intervention object from original image
                    $img = resize_custom_image($img, 40, 40); // resize; found in helpers.php
                    File::makeDirectory(public_path($destination) . '/37-51', 0775, FALSE, TRUE);
                    $img->save($path); // store image 
                    $values = array('id' => $TourTypeLogo_id);

                    $update_data = array('logo_path' => $path);
                    $TourTypeLogoUpdate = TourTypeLogo::where('id', $TourTypeLogo_id)->update($update_data);
                }

                Session::flash('message', "Record Successfully added!");
                return Redirect::back();
            }
        } 
        $oTourTypeLogo = TourTypeLogo::find($nIdTourLogo);
        return \View::make('WebView::tour.tour_logo_create', compact('oTourTypeLogo','sSearchStr','sOrderField','sOrderBy','nIdTourLogo'));
    }
    
    public function callTourTypeLogoDelete($nIdTourLogo) 
    {
        $oTourTypeLogo = TourTypeLogo::find($nIdTourLogo);
        File::delete( public_path( $oTourTypeLogo->logo_path ) );
        $oTourTypeLogo->delete();
        Session::flash('message', "Record Successfully Deleted!");
        return Redirect::back();
    }
}
    