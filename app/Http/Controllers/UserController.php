<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;

use App\User;
use App\Licensee;
use App\AgentCommissionPercentage;
use App\Agent;
use App\Currency;
use App\Customer;
use Auth;
use Hash;
use DB;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['callUserList', 'callCreateUserLicensee', 'checkUserNameExists', 'getRandomPassword',
                                                'callCreateUserAgent','callCreateUserCustomer']]);
    }
    
    public function callUserlogin(Request $oRequest)
    {
    	if ($oRequest->isMethod('post'))
    	{
            $aData =  $oRequest->all();
            $sUser = $this->loginUser($aData);
            //if login is correct
            if ($sUser['auth'] == 'valid') {

                    Auth::login($sUser);
                    return Redirect::to('/');

            } elseif ($sUser['auth'] == 'inactive') {
                    // if account is not active
                    return Redirect::back()->withErrors('Account not active.');
            } else {
                    //if invalid credentials
                    return Redirect::back()->withErrors('Invalid username or password.');
            }
            return "this is a test";
        }
        return view('WebView::home.login');
    }
    
    public function callUserLogout() 
    {
        Auth::logout();
        return redirect('/login');
    }

    public function callUserList(Request $oRequest, $sUserType='')
    {
        //remove session when it comes from sidebar
        if(session('page_name') != $sUserType)
            $oRequest->session()->forget('user');
        
       // echo $oRequest->has('search_str');
        session(['page_name' => $sUserType]);
        $aData = session('user') ? session('user') : array();
        $oRequest->session()->forget('user');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'name');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        
        if(count($aData) && $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oUserList = User::getUserList($sUserType,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        //dd(DB::getQueryLog());

        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oUserList->currentPage(),'user');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::user._more_user_list';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::user.user_list' : 'WebView::user._user_list_ajax';
        
        return \View::make($oViewName, compact('oUserList','sUserType','sSearchStr','sOrderField','sOrderBy','nShowRecord'));
    }
    
    public function callCreateUserLicensee(Request $oRequest)
    {
        session(['page_name' => 'licensee']);
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'name' => 'required',
                                    'domain' => 'required|unique:licensees,domain',
                                    'username' => 'required|email|unique:users,username',
                                    'password' => 'required|min:6',
                                    'duration' => 'required'
                                    ]);
            if($oValidator->fails()) 
            {
                //print_r($oValidator->messages());exit;
                return Redirect::back()->withErrors($oValidator)->withInput( $oRequest->except( 'password' ) );
            }
            $oUser = User::firstOrCreate([
				'name' => $oRequest->name,
				'username' => $oRequest->username,
				'password' => Hash::make( $oRequest->password ),
				'type' => config('constants.USERTYPELICENSEE'),
				'active' => 0,
			]);
            $oLicensee = Licensee::firstOrCreate([
				'user_id' => $oUser->id,
				'domain' => $oRequest->domain,
				'expiry_date' => date( 'Y-m-d', strtotime( '+' . $oRequest->duration .' days' ) )
			]);
            return Redirect::back()->with( [
				'message' => 'success', 
				'user_id' => $oUser->id, 
				'licensee_name' => $oUser->name 
				]);
        }
        return \View::make('WebView::user.user_create_licensee');
    }
    
    public function checkUserNameExists(Request $oRequest) 
    {
        $result = User::where(['username' => $oRequest->username ])->first();
        return (count($result) > 0) ? 0: 1;
    }
    
    private function getRandomPassword() 
    {
        $length = 8; // pw length
    	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    	$password = substr( str_shuffle( $chars ), 0, $length );
    	return $password;
    }
    
    public function callCreateUserAgent(Request $oRequest)
    {
        session(['page_name' => 'agent']);
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'first_name'     => 'required',
                                    'last_name'      => 'required',
                                    'username'       => 'required|unique:users,username',
                                    'contact_number' => 'required',
                                    'address'        => 'required',
                                    'percentage'     => 'required|numeric'
                                    ]);
            if($oValidator->fails()) 
            {
                //print_r($oValidator->messages());exit;
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            $sPassword = '123456'; //$this->getRandomPassword();
            $oUser = User::firstOrCreate([
				'name' => $oRequest->first_name.' '.$oRequest->last_name,
				'username' => $oRequest->username,
				'password' => Hash::make( $sPassword ),
				'type' => config('constants.USERTYPEAGENT'),
				'active' => 1,
			]);
            $oAgent = Agent::firstOrCreate([
				'user_id' => $oUser->id,
				'contact_number' => $oRequest->contact_number,
                                'address' => $oRequest->address
			]);
            $oAgentCommissionPercentage = AgentCommissionPercentage::firstOrCreate([
                                                    'percentage' => $oRequest->percentage,
                                                    'agent_id' => $oAgent->id
                                            ]);
            Agent::where(['id' => $oAgent->id])->update(['agent_commission_percentage_id' => $oAgentCommissionPercentage->id]);
            return Redirect::back()->with( [
				'message' => 'success', 
				'user_id' => $oUser->id, 
				'agent_name' => $oUser->name 
				]);
        }
        return \View::make('WebView::user.user_create_agent');
    }
    
    public function callCreateUserCustomer(Request $oRequest) 
    {
        session(['page_name' => 'customer']);
        $oCurrency = Currency::all();
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'first_name'  => 'required',
                                    'last_name'   => 'required',
                                    'username'    => 'required|unique:users,username',
                                    'currency_id' => 'required',
                                    'address'     => 'required'
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            $sPassword = '123456'; //$this->getRandomPassword();
            $oUser = User::firstOrCreate([
				'name' => $oRequest->first_name.' '.$oRequest->last_name,
				'username' => $oRequest->username,
				'password' => Hash::make( $sPassword ),
				'type' => config('constants.USERTYPECUSTOMER'),
				'active' => 1,
			]);
            $oCurrencyCode = Currency::select('code')->where('id', $oRequest->currency_id)->first();
            
            $customer = Customer::firstOrCreate([
                'first_name' =>$oRequest->first_name,
                'last_name' =>$oRequest->last_name,
                'user_id' => $oUser->id,
                'email' => $oRequest->username,
                'currency' =>$oCurrencyCode->code
            ]);
            
            return Redirect::back()->with( [
				'message' => 'success', 
				'user_id' => $oUser->id, 
				'agent_name' => $oUser->name 
				]);
        }
        return \View::make('WebView::user.user_create_customer', compact('oCurrency'));
    }
    
//    public function test1(Request $oRequest) {
//        if ($oRequest->isMethod('post'))
//    	{
//            $aArray['from_activity'] = 1;
//            return redirect()->route('user.test2',[1,2]);
//        }
//        return \View::make('WebView::home.test');
//    }
//    public function test2(Request $oRequest,$id='',$data='') {
//        echo $id;
//        return \View::make('WebView::home.login');
//    }
}
