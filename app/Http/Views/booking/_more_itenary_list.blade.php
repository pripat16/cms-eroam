@foreach ($oItineraries as $aItinerary)  
    <?php $name ='Guest'; ?>
    @foreach ($aItinerary['Passenger'] as $key => $value) 
        <?php $name = ($value->is_lead == 'Yes') ? $value->first_name . ' ' . $value->last_name : 'Guest'; ?>
    @endforeach
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-{{$aItinerary->order_id}}">
                <input type="checkbox" id="checkbox-{{$aItinerary->order_id}}" value="{{ $aItinerary->order_id }}" class="cmp_check">&nbsp;
            </label>
        <td> <a href="{{ route('booking.show-itenary',['nItenaryId'=>$aItinerary->order_id]) }}">{{$aItinerary->invoice_no}}</a></td>
        <td>{{ date( 'd/m/y', strtotime( $aItinerary->created_at ) ) }}</td>
        <td></td>
        <td>{{ $name }}</td>
        <td>
            <span data-status-id="{{ $aItinerary->order_id }}" >
                <a href="javascript://" title="Update Status" class="update-status-btn"><i class="fa fa-pencil-square-o"></i></a> 
                <span class="status-name">{{ $aItinerary->status }}</span>
            </span>
            <div class="update-status-box"></div>
        </td>
        <td>{{ $aItinerary->currency }}$ {{ $aItinerary->total_amount }}</td>
        <td>
            <span data-voucher-id="{{ $aItinerary->order_id }}" >
                <a href="javascript://" title="Update Voucher" class="update-voucher-btn"><i class="fa fa-pencil-square-o"></i></a> 
                <span class="voucher-name">{{ $aItinerary->voucher }}</span>
            </span>
            <div class="update-voucher-box"></div>
        </td>
        <td class="text-center">
            <a href="{{ route('booking.show-itenary',['nItenaryId'=>$aItinerary->order_id])}}" class="button success tiny btn-primary btn-sm">View</a>
        </td>
    </tr>
@endforeach