<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>:: eroam ::</title>
        <style type="text/css">
            body{font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; font-size: 14px; color: #212121;}
            .content-container{width: 700px; margin: 0 auto;}
            .box-wrapper {
                padding: 20px 30px 20px 30px;
                position: relative;
                margin-bottom: 20px;
                background: #FFFFFF;
                border: 1px solid #F8F8F8;
                box-shadow: 0 0 2px 0 rgba(0,0,0,0.12), 0 2px 2px 0 rgba(0,0,0,0.24);
                -webkit-box-shadow: 0 0 2px 0 rgba(0,0,0,0.12), 0 2px 2px 0 rgba(0,0,0,0.24);
                -moz-box-shadow: 0 0 2px 0 rgba(0,0,0,0.12), 0 2px 2px 0 rgba(0,0,0,0.24);
                -ms-box-shadow: 0 0 2px 0 rgba(0,0,0,0.12), 0 2px 2px 0 rgba(0,0,0,0.24);
                border-radius: 2px;
            }
            .row::after{clear: both; display: block; content: "";}
            .box-wrapper p{margin: 0 0 10px 0;}
            .m-t-20{margin-top: 20px;}
        </style>

    </head>
    <body style="font-family: "Helvetica Neue, Helvetica,Arial,sans-serif; font-size: 14px; color: #212121;">
          <div class="content-container" style="width: 700px; margin: 0 auto;">

            <h1 style="font-size: 20px; color: #212121; letter-spacing: 0.71px; line-height: 18px; margin-top: 10px; margin-bottom: 17px;">Booking Information</h1> 


            <?php
            $itinerary = $aData['itinerary'];
            $name = 'Guest';
            foreach ($itinerary['Passenger'] as $key => $value) {
                if ($value->is_lead == 'Yes') {
                    $name = $value->first_name . ' ' . $value->last_name;
                    $email = $value->email;
                    $contact = $value->contact;
                    $gender = $value->gender;
                    $dob = date('d-m-Y', strtotime($value->dob));

                    foreach ($value['Country'] as $key1 => $value1) {
                        $country = $value1->name;
                    }
                }
            }
            ?>
            <div class="box-wrapper" style="padding: 20px 30px 20px 30px;position:relative;margin-bottom:20px;background:#FFFFFF;border:2px solid #efefef;border-radius: 2px;">
                <table border="0" width="100%" cellspacing="4" cellpadding="4">
                    <tr>
                        <td colspan="2">{{ trans('messages.booking_id') }}: {{ $itinerary['invoice_no'] }}</td>
                    </tr>
                    <tr>
                        <td width="50%">{{ trans('messages.customer_name') }}: {{$name}}</td>
                        <td width="50%">{{ trans('messages.customer_email') }}: {{$email}}</td>
                    </tr>
                    <tr>
                        <td width="50%">{{ trans('messages.customer_contact') }}: {{$contact}}</td>
                        <td width="50%">{{ trans('messages.customer_gender') }}: {{$gender}}</td>
                    </tr>
                    <tr>
                        <td width="50%">{{ trans('messages.customer_dob') }}: {{$dob}}</td>
                        <td width="50%">{{ trans('messages.customer_country') }}: {{$country}}</td>
                    </tr>
                    <tr>
                        <td width="50%">{{ trans('messages.days') }}: {{intval($itinerary['total_days'])}}</td>
                        <td width="50%">{{ trans('messages.total_amount') }}: {{$itinerary['currency']}} {{$itinerary['cost_per_day']}} per day (TOTAL {{$itinerary['currency']}} {{$itinerary['total_per_person']}} (per person))</td>

                    </tr>
                    <tr>
                        <td width="50%">{{ trans('messages.date') }}: {{ date( 'j M Y', strtotime($itinerary['from_date']))}} - {{ date( 'j M Y',strtotime($itinerary['to_date']))}}</td>
                        <td width="50%">{{ trans('messages.travellers') }}: {{$itinerary['num_of_travellers']}}</td>

                    </tr>
                </table>
            </div>
            <?php
            $last_key = count($itinerary['ItenaryLegs']) - 1;
            foreach ($itinerary['ItenaryLegs'] as $key => $value) {
                ?>
                <div class="box-wrapper" style="padding: 20px 30px 20px 30px;position:relative;margin-bottom:20px;background:#FFFFFF;border:2px solid #efefef;border-radius: 2px;">
                    <table border="0" width="100%" cellspacing="4" cellpadding="4">
                        <tr>
                            <td colspan="2">{{$value['from_city_name']}}, {{$value['country_code']}}</td>
                        </tr>


    <?php
    foreach ($itinerary['LegDetails'] as $key1 => $value1) {
        if ($value['itenary_order_id'] == $value1['itenary_order_id'] && $value['itenary_leg_id'] == $value1['itenary_leg_id']) {
            if ($value1['leg_type'] == 'hotel') {
                if ($value1['leg_name'] != 'Own Arrangement') {
                    ?>
                                        <tr>
                                            <td colspan="2"><img src="{{ URL::to('images/hotel-icon.png') }}" alt="" style="color: #000;"/>{{ trans('messages.booking_accommodation') }}</td>
                                        </tr>

                                        <tr>
                                            <td width="50%">{{ trans('messages.booking_accommodation_name') }}: {{$value1['leg_name']}}</td>
                                            <td width="50%">{{$value1['nights']}} {{ trans('messages.night') }}</td>
                                        </tr>
                                        <tr>
                                            <td width="50%">{{ trans('messages.check_in') }}: {{ date( 'j M Y', strtotime($value1['from_date']))}}</td>
                                            <td width="50%">{{ trans('messages.check_out') }}: {{ date( 'j M Y', strtotime($value1['to_date']))}}</td>
                                        </tr>
                                        <tr>
                                            <td width="50%">{{ trans('messages.price') }}: {{$value1['currency']}} {{ceil($value1['price'])}}</td>
                                            <td width="50%">{{ trans('messages.booking_id') }}: {{$value1['booking_id']}}</td>
                                        </tr>
                    <?php
                    if (!empty($value1['provider'])) {
                        ?>
                                            <tr>
                                                <td width="50%">{{ trans('messages.provider_name') }}: {{$value1['provider']}}</td>
                                            </tr>
                        <?php
                    } else {
                        ?>
                                            <tr>
                                            <?php
                                            $name = "";
                                            if (isset($value1->HotelSuppliers[0]) && !empty($value1->HotelSuppliers[0])) {
                                                $name = $value1->HotelSuppliers[0]['name'];
                                            }
                                            ?>
                                                <td width="50%">{{ trans('messages.provider_name') }}: {{$name}}</td>
                                                <?php
                                                $phone = "";
                                                if (isset($value1->HotelSuppliers[0]) && !empty($value1->HotelSuppliers[0])) {
                                                    $phone = $value1->HotelSuppliers[0]['accounts_contact_phone'];
                                                }
                                                ?>
                                                <td width="50%">{{ trans('messages.booking_id') }}: {{$phone}}</td>
                                            </tr>
                                            <tr>
                        <?php
                        $email = "";
                        if (isset($value1->HotelSuppliers[0]) && !empty($value1->HotelSuppliers[0])) {
                            $email = $value1->HotelSuppliers[0]['accounts_contact_email'];
                        }
                        ?>  
                                                <td width="50%">{{ trans('messages.provider_email') }}: {{$email}}</td>
                                                <td width="50%">{{ trans('messages.booking_status') }}: {{!empty($value1['provider_booking_status']) ? $value1['provider_booking_status'] : 'NOT CONFIRMED'}}</td>
                                            </tr>
                        <?php
                    }
                    ?>
                                        <tr>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                        </tr> 
                    <?php
                } else {
                    ?>
                                        <tr>
                                            <td colspan="2"><img src="{{ URL::to('images/hotel-icon.png') }}" alt="" style="color: #000;"/>{{ trans('messages.accommodation') }}</td>
                                        </tr>
                                        <tr>
                                            <td width="50%">{{$value1['leg_name']}}</td>
                                            <td width="50%">{{ trans('messages.booking_id') }}: {{$value1['booking_id']}}</td>
                                        </tr> 
                                        <tr>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                        </tr> 
                    <?php
                }
            }
            if ($value1['leg_type'] == 'activities') {
                if ($value1['leg_name'] != 'Own Arrangement') {
                    ?>


                                        <tr>
                                            <td colspan="2"><img src="{{ URL::to('images/activity-icon.png') }}" alt="" style="color: #000;"/>{{ trans('messages.activity_summary') }} </td>
                                        </tr>
                                        <tr>
                                            <td width="50%">{{ trans('messages.activity_name') }}: {{$value1['leg_name']}}</td>
                                            <td width="50%">{{ trans('messages.date') }}: {{ date( 'j M Y', strtotime($value1['from_date']))}}</td>
                                        </tr>
                                        <tr>
                                            <td width="50%">{{ trans('messages.price') }}: {{$value1['currency']}} {{ceil($value1['price'])}}</td>
                                            <td width="50%">{{ trans('messages.booking_id') }}: {{$value1['booking_id']}}</td>
                                        </tr>
                    <?php
                    if (!empty($value1['provider'])) {
                        ?>
                                            <tr>
                                                <td width="50%">{{ trans('messages.provider_name') }}: {{$value1['provider']}}</td>
                                                <td width="50%">{{ trans('messages.booking_status') }}: {{!empty($value1['booking_id']) ? 'CONFIRMED' : 'NOT CONFIRMED'}}</td>
                                            </tr>
                        <?php
                        if (!empty($value1['booking_error_code'])) {
                            ?>  
                                                <tr>
                                                    <td width="50%">{{ trans('messages.booking_error_code') }}: {{$value1['booking_error_code']}}</td>
                                                    <td width="50%">{{ trans('messages.booking_error_message') }}: {{$value1['provider_booking_status']}}</td>
                                                </tr>
                            <?php
                        }

                        if (!empty($value1['voucher_url'])) {
                            ?>  
                                                <tr>
                                                    <td width="50%">{{ trans('messages.voucher_url') }}: 
                                                        <a style="color: #337ab7;text-decoration: underline;" href="{{$value1['voucher_url']}}" target="_blank">{{ trans('messages.click_here') }}</a>
                                                    </td>
                                                </tr>
                            <?php
                        }
                    } else {
                        ?>
                                            <tr>
                                            <?php
                                            $name = "";
                                            if (isset($value1->ActivitySuppliers[0]) && !empty($value1->ActivitySuppliers[0])) {
                                                $name = $value1->ActivitySuppliers[0]['name'];
                                            }
                                            ?>  
                                                <td width="50%">{{ trans('messages.provider_name') }}: {{$name}}</td>
                                                <?php
                                                $phone = "";
                                                if (isset($value1->ActivitySuppliers[0]) && !empty($value1->ActivitySuppliers[0])) {
                                                    $phone = $value1->ActivitySuppliers[0]['accounts_contact_phone'];
                                                }
                                                ?> 
                                                <td width="50%">{{ trans('messages.provider_contact') }}: {{$phone}}</td>
                                            </tr>
                                            <tr>
                        <?php
                        $email = "";
                        if (isset($value1->ActivitySuppliers[0]) && !empty($value1->ActivitySuppliers[0])) {
                            $email = $value1->ActivitySuppliers[0]['accounts_contact_email'];
                        }
                        ?> 
                                                <td width="50%">{{ trans('messages.provider_email') }}: {{$email}}</td>
                                                <td width="50%">{{ trans('messages.booking_status') }}: {{!empty($value1['provider_booking_status']) ? $value1['provider_booking_status'] : 'NOT CONFIRMED'}}</td>
                                            </tr>
                        <?php
                    }
                    ?>
                                        <tr>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                        </tr> 
                    <?php
                } else {
                    ?>
                                        <tr>
                                            <td colspan="2"><img src="{{ URL::to('images/activity-icon.png') }}" alt="" style="color: #000;"/>{{ trans('messages.activity_summary') }}</td>
                                        </tr>
                                        <tr>
                                            <td width="50%">{{$value1['leg_name']}}</td>
                                            <td width="50%">{{ trans('messages.booking_id') }}: {{$value1['booking_id']}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                        </tr> 
                    <?php
                }
            }
            if ($value1['leg_type'] == 'transport') {
                if ($value1['leg_name'] != 'Own Arrangement') {

                    $depart = $value1['departure_text'];
                    $arrive = $value1['arrival_text'];
                    if ($value1['leg_name'] == 'Flight') {
                        if (!empty($value1['booking_summary_text'])) {
                            $leg_depart = explode("Depart:", $value1['booking_summary_text'], 2);
                            $leg_arrive = explode("Arrive:", $value1['booking_summary_text'], 2);

                            if (isset($leg_depart[1])) {
                                $depart = explode(") )", $leg_depart[1], 2);
                                $depart = $depart[0] . ') )';
                            } else {
                                $depart = '';
                            }
                            if (isset($leg_arrive[1])) {
                                $arrive = explode(") )", $leg_arrive[1], 2);
                                $arrive = $arrive[0] . ') )';
                            } else {
                                $arrive = '';
                            }
                        } else {
                            $depart = '';
                            $arrive = '';
                        }
                    }
                    ?>
                                        <tr>
                                            <td colspan="2"><img src="{{ public_path('images\transport-icon.png') }}" alt="" style="color: #000;"/>{{ trans('messages.transport') }}</td>
                                        </tr>  
                                        <tr>
                    <?php
                    $leg_name = explode('Depart:', $value1['booking_summary_text']);
                    if (isset($value1['booking_summary_text']) && !empty($value1['booking_summary_text'])) {
                        $leg_name = trim(strip_tags($leg_name[0]));
                    } else {
                        $leg_name = $value1['leg_name'];
                    }
                    ?>
                                            <td colspan="2">{{ trans('messages.service') }}: {{$leg_name}}</td>
                                        </tr>  
                                        <tr>
                                            <td width="50%">{{ trans('messages.depart') }}: {!!$depart !!}</td>
                                            <td width="50%">{{ trans('messages.arrive') }}: {{$arrive}}</td>
                                        </tr>
                                        <tr>
                                            <td width="50%">{{ trans('messages.price') }}: {{$value1['currency']}} {{ceil($value1['price'])}}</td>
                                            <td width="50%">{{ trans('messages.booking_id') }}: {{$value1['booking_id']}}</td>
                                        </tr>
                    <?php
                    if (!empty($value1['provider'])) {
                        ?>
                                            <tr>
                                                <td width="50%">{{ trans('messages.provider_name') }}: {{$value1['provider']}}</td>
                                                <td width="50%">{{ trans('messages.booking_status') }}: {{!empty($value1['booking_id']) ? 'CONFIRMED' : 'NOT CONFIRMED'}}</td>
                                            </tr>
                        <?php
                        if (!empty($value1['booking_error_code'])) {
                            ?>  
                                                <tr>
                                                    <td width="50%">{{ trans('messages.booking_error_code') }}: {{$value1['booking_error_code']}}</td>
                                                    <td width="50%">{{ trans('messages.booking_error_message') }}: {{$value1['provider_booking_status']}}</td>
                                                </tr>
                            <?php
                        }
                    } else {
                        ?>
                                            <tr>
                                                <td width="50%">{{ trans('messages.provider_name') }}: {{isset($value1->TransportSuppliers[0]['name'])? $value1->TransportSuppliers[0]['name']:''}}</td>
                                                <td width="50%">{{ trans('messages.provider_contact') }}: {{isset($value1->TransportSuppliers[0]['accounts_contact_phone'])? $value1->TransportSuppliers[0]['accounts_contact_phone']:''}}</td>
                                            </tr>
                                            <tr>
                                                <td width="50%">{{ trans('messages.provider_email') }}: {{isset($value1->HotelSuppliers[0]['TransportSuppliers'])? $value1->TransportSuppliers[0]['accounts_contact_email']:''}}</td>
                                                <td width="50%">{{ trans('messages.booking_status') }}: {{!empty($value1['provider_booking_status']) ? $value1['provider_booking_status'] : 'NOT CONFIRMED'}}</td>
                                            </tr>
                        <?php
                    }
                    ?>
                                        <?php
                                    } else {
                                        if ($key != $last_key) {
                                            ?>
                                            <tr>
                                                <td colspan="2"><img src="{{ URL::to('images/transport-icon.png') }}" alt="" style="color: #000;"/>{{ trans('messages.transport') }}</td>
                                            </tr>  
                                            <tr>
                                                <td width="50">{{ trans('messages.self_drive') }}: {{ trans('messages.transport') }}</td>
                                                <td width="50">{{ trans('messages.booking_id') }}: {{$value1['booking_id']}}</td>
                                                </p> 
                                            </tr>
                        <?php
                    }
                }
            }
        }
    }
    ?>
                    </table>
                </div>
                        <?php
                    }
                    ?>
        </div>
    </body>
</html>