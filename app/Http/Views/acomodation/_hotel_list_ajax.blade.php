<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getCountrySort(this,'h.name');">{{ trans('messages.acomodation_name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'c.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCountrySort(this,'h.eroam_code');">{{ trans('messages.eroam_code') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'c.optional_city')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCountrySort(this,'h.supplier ');"> {{ trans('messages.supplier') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'co.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getCountrySort(this,'h.address_1');"> {{ trans('messages.address') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'h.address')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th class="text-center">{{ trans('messages.status') }} </th>
        </tr>
    </thead>
    <tbody class="hotel_list_ajax">
    @if(count($oHotelList) > 0)
        @include('WebView::acomodation._more_hotel_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oHotelList->count() , 'total'=>$oHotelList->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $oHotelList->lastPage() }},
            currentPage: {{ $oHotelList->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                if(pageNumber > 1)
                    callHotelListing(event,'hotel_list_ajax',pageNumber);
                else
                    callHotelListing(event,'table_record',pageNumber);
                $('#checkbox-00').prop('checked',false);
                setupLabel();
            }
        });
    });
</script>