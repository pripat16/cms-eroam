<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getUserSort(this,'name');">{{ trans('messages.name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getUserSort(this,'created_at');"> {{ trans('messages.created_at') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'created_at')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getUserSort(this,'updated_at');"> {{ trans('messages.updated_at') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'updated_at')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody class="label_list_ajax">
    @if(count($oLabelList) > 0)
        @include('WebView::common._more_label_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oLabelList->count() , 'total'=>$oLabelList->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $oLabelList->lastPage() }},
            itemsOnPage: 10,
            currentPage: {{ $oLabelList->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                if(pageNumber > 1)
                    callLabelListing(event,'label_list_ajax',pageNumber);
                else
                    callLabelListing(event,'table_record',pageNumber);
                $('#checkbox-00').prop('checked',false);
                setupLabel();
            }
        });
    });
</script>