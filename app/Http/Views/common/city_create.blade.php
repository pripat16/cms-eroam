@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">
    <h1 class="page-title">{{ trans('messages.add_new_city') }}</h1>
    <div class="row">
      @if (Session::has('message'))
        <div class="small-6 small-centered columns success-box">{{ Session::get('message') }}</div>
      @endif
    </div>
    <br>

    <form method="POST" action="{{ route('common.create-city') }}" accept-charset="UTF-8" enctype="multipart/form-data" id="create">
        {{ csrf_field() }}
        <input type="hidden" name="city_id" value="{{ $nIdCity }}" >
        <div class="box-wrapper">
            <p>{{ trans('messages.city_details') }}</p>
            <div class="form-group m-t-30">
                <label class="label-control"> {{ trans('messages.city_name') }} <span class="required">*</span></label>
                <input id="name" class="form-control" placeholder="{{ trans('messages.city_name_placeholder') }}" name="name" type="text" value="{{($oCity) ? $oCity->name : old('name')}}">
                @if ( $errors->first( 'name' ) )
                    <small class="error">{{ $errors->first('name') }}</small>
                @endif
            </div>

            <div class="form-group m-t-30">
                <label class="label-control">{{ trans('messages.country_name') }} <span class="required">*</span></label>
                <select class="form-control" name="country_id">
                    <option value="">{{ trans('messages.country_name_placeholder') }}</option>
                    @foreach($oCountry as $aCountry)
                        <?php $sSelectedCountry = ''; ?>
                        @if($oCity && $oCity->country->id == $aCountry->id)
                            <?php $sSelectedCountry = 'selected'; ?>
                        @elseif(old('country_id') == $aCountry->id)
                            <?php $sSelectedCountry = 'selected'; ?>
                        @endif
                        <option value="{{ $aCountry->id }}" {{ $sSelectedCountry }}>{{ $aCountry->name }}</option>
                    @endforeach
                </select>  
                @if ( $errors->first( 'country_id' ) )
                    <small class="error">{{ $errors->first('country_id') }}</small>
                @endif
            </div>

            <div class="form-group m-t-30">
                <label class="label-control"> {{ trans('messages.default_nights') }}<span class="required">*</span></label>
                <input id="default_nights" class="form-control" placeholder="{{ trans('messages.default_nights_placeholder') }}" min="0" name="default_nights" type="number" value="{{($oCity) ? $oCity->default_nights : old('default_nights')}}">
                @if ( $errors->first( 'default_nights' ) )
                    <small class="error">{{ $errors->first('default_nights') }}</small>
                @endif
            </div>

            <div class="form-group m-t-30">
                <label class="label-control">{{ trans('messages.optional_city') }} </label>
                <div>
                    <label class="radio-checkbox label_radio" for="radio-03">
                        <input type="radio" id="radio-03" value="1" name="optional_city" {{ ($oCity && $oCity->optional_city == 1) ? 'checked' : '' }}> Yes
                    </label> 
                    <label class="radio-checkbox label_radio r_on" for="radio-04">
                        <input type="radio" id="radio-04" value="0" name="optional_city" {{ ($oCity && $oCity->optional_city == 0) ? 'checked' : '' }}> No
                    </label>
                </div>
            </div>

            <div class="form-group m-t-30">
                <label class="label-control">{{ trans('messages.enabled') }}</label>
                <div>
                    <input id="exampleCheckboxSwitch" type="checkbox" name="enabled" {{ ($oCity && $oCity->optional_city == 0) ? '' : 'checked' }}>
                    <label for="exampleCheckboxSwitch" style="display:none;"></label>
                </div>
            </div>
            @if($nIdCity != '')
            <div class="form-group m-t-30">
                <label class="label-control">{{ trans('messages.timezone') }} <span class="required">*</span></label>
                <?php //echo "<pre>"; print_r($oCity->timezone);exit; ?>
                <select class="form-control" name="timezone_id">
                    <option value="" selected="selected">{{ trans('messages.timezone_placeholder') }}</option>
                    @foreach($oTimezone as $aTimezone)
                        <?php $sSelectedCountry = ''; ?>
                        @if($oCity && $oCity->timezone->id == $aTimezone->id)
                            <?php $sSelectedCountry = 'selected'; ?>
                        @elseif(old('timezone_id') == $aTimezone->id)
                            <?php $sSelectedCountry = 'selected'; ?>
                        @endif
                        <option value="{{ $aTimezone->id }}" {{ $sSelectedCountry }}>{{ $aTimezone->name }}</option>
                    @endforeach
                </select>  
                @if ( $errors->first( 'timezone_id' ) )
                    <small class="error">{{ $errors->first('timezone_id') }}</small>
                @endif
            </div>
            @endif
        </div>

        <div class="box-wrapper"> 
            <p>{{ trans('messages.city_description') }} <span class="required">*</span></p>  
            <div class="form-group m-t-30">
                <textarea name="description" placeholder="{{ trans('messages.city_description_placeholder') }}" rows="5" id="description" class="form-control" >         
                {{($oCity) ? $oCity->description : old('description')}}
                </textarea>		
            </div>
            @if ( $errors->first( 'description' ) )
                <small class="error">{{ $errors->first('description') }}</small>
            @endif
        </div>

        <div class="box-wrapper">
            <p>{{ trans('messages.map_location') }}</p>

            <div class="form-group m-t-30">
                <label class="label-control">{{ trans('messages.latitude_placeholder') }} <span class="required">*</span></label>
                <input id="lat" class="form-control" placeholder="{{ trans('messages.latitude_placeholder') }}" name="lat" type="text" value="{{($oCity) ? $oCity->latlong->lat : old('lat')}}">		
            </div> 
            @if ( $errors->first( 'lat' ) )
                <small class="error">{{ $errors->first('lat') }}</small>
            @endif
            <div class="form-group m-t-30">
                <label class="label-control">{{ trans('messages.longitude') }} <span class="required">*</span></label>
                <input id="lng" class="form-control" placeholder="{{ trans('messages.longitude_placeholder') }}" name="lng" type="text" value="{{($oCity) ? $oCity->latlong->lng : old('lng')}}">
            </div>
            @if ( $errors->first( 'lng' ) )
                <small class="error">{{ $errors->first('lng') }}</small>
            @endif  
        </div>
        
        @if($nIdCity != '')
            <div class="box-wrapper">
                <p>{{ trans('messages.hb_destination_mapping') }}</p>

                <div class="row hb-mapping-container">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.hb_destinations') }}
                                <span data-tooltip aria-haspopup="true" class="has-tip" style="vertical-align: sub;" id="has-hb-tool-tip" title="{{ trans('messages.hb_title') }}">
                                    <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                                </span>
                                </span>
                            </label>
                            <select class="form-control" id="hb-destinations" multiple></select>
                            <input type="hidden" name="hb_destination_codes" value="[]"/>   
                        </div>
                    </div>  
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{!! trans('messages.select_hb') !!}</label>
                            <div id="selected-hb-destinations"></div>
                        </div>
                    </div>  
                </div>
            </div>

            <div class="box-wrapper">
                <p>{{ trans('messages.aot_location_mapping') }}</p>

                <div class="row aot-mapping-container">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.aot_locations') }}
                                <span data-tooltip aria-haspopup="true" class="has-tip" style="vertical-align: sub;" id="has-aot-tool-tip" title="{{ trans('messages.aot_location_title') }}">
                                    <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                                </span>
                            </label>
                            <select class="form-control" id="aot-supplier-location-type">
                                <option value="S">States</option>
                                <option value="Z">Super Regions</option>
                                <option value="R">Regions</option>
                                <option value="T" selected="selected">Towns</option>
                            </select>
                        </div>
                        <div class="form-group m-t-10">
                            <select class="form-control" id="aot-supplier-locations" multiple></select>
                            <input type="hidden" name="aot_location_codes" value=""/>
                        </div>
                    </div>	
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{!! trans('messages.select_aot') !!}</label>
                            <div id="selected-aot-locations"></div>
                        </div>
                    </div>  

                </div>
            </div>  

            <div class="box-wrapper">
                <p>{{ trans('messages.viator_destination_mapping') }}</p>
                <div class="row viator-mapping-container">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.viator_destinations') }}
                                <span data-tooltip aria-haspopup="true" class="has-tip" style="vertical-align: sub;" title="{{ trans('messages.viator_destinations_title') }}">
                                    <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                                </span>
                            </label>
                            <select class="form-control" id="viator-destination-type"></select>
                        </div>
                        <div class="form-group m-t-10">

                            <select class="form-control" id="viator-destinations" multiple></select>
                            <input type="hidden" name="viator_destination_ids" value="[]"/>
                        </div>
                    </div>  
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{!! trans('messages.select_viator') !!}</label>
                            <div id="selected-viator-destinations"></div>
                        </div>
                    </div>  
                </div>
            </div>

            <div class="box-wrapper">
                <p>{{ trans('messages.ae_city_mapping') }}</p>

                <div class="row ae-mapping-container">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.ae_cities') }}
                                <span data-tooltip aria-haspopup="true" class="has-tip" style="vertical-align: sub;" title="{{ trans('messages.ae_cities_title') }}">
                                    <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                                </span>
                            </label>
                            <select class="form-control" id="ae-regions" multiple></select>
                            <input type="hidden" name="ae_region_ids" value="[]"/>   
                        </div>

                    </div>  
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{!! trans('messages.select_ae') !!}</label>
                            <div id="selected-ae-regions"></div>
                        </div>
                    </div>  

                </div>
            </div>
        @endif
        <div class="row">
            <div class="m-t-20  col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="col-sm-6">
                        <input class="button success btn btn-primary btn-block" type="submit" value="{{ trans('messages.save_btn') }}">
                    </div>
                    <div class="col-sm-6">
                      <a href="{{ route('common.city-list') }}" class="btn btn-primary btn-block">{{ trans('messages.cancel_btn') }}</a>
                    </div>
                </div>
             </div>
        </div>
    </form>	
    
    @if($nIdCity != '')
        <div class="box-wrapper m-t-30">
            <p>City Images</p>
            <form enctype="multipart/form-data" id="image-form">
                {{ csrf_field() }}
                <div class="file-upload">
                    <input type="hidden" name="city_id" value="{{ $nIdCity }}"/>
                    <input type="file" class="file-input" id="image" name="city_image[]" accept="image/gif,image/jpeg,image/jpg,image/png" />Upload Image
                </div>
                <span class="input-filename"></span>
                <span id="image-submit-load" style="display:none;">
                    <i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw image-file-loader"></i>
                </span>  
            </form>
            <hr/>
            <div class="image-alert-box-container">
                <div data-alert class="alert-box image-alert-box" style="display:none;">
                    <span class="image-alert-message"></span>
                    <button class="image-alert-box-close white">&times;</button>
                </div>
            </div>
            <ul class="clearing-thumbs small-block-grid-6">
                <!-- check if an image is existing for this city -->
                <?php if ($oCity->image): ?> 

                    <!-- loop through each image for this city -->
                    <?php foreach ($oCity->image as $key => $image): ?> 

                        <li data-id="{{ $image->id }}">
                            <!-- check if file exists for column "path" stored in table "zcityImages" -->

                            <?php if (File::exists($image->thumbnail)): ?>
                                <a href="{{ url($image->large) }}" class="th">
                                    <?php if ($image->is_primary == TRUE || count($oCity->image) <= 1): ?>
                                        <span class="primary-image-container" data-id="{{ $image->id }}">
                                            <i class="fa fa-user fa-sm primary-image" aria-hidden="true"></i>
                                        </span>
                                    <?php endif; ?>   
                                    <img src="{{ url($image->thumbnail) }}">
                                </a>
                                <div style="padding:2px">
                                    <div class="row" style="margin-top:4px;">

                                        <!-- check if image is the primary picture or if number of images returned is greater than 1-->
                                        <?php if ($image->is_primary == TRUE || count($oCity->image) <= 1): ?>
                                            <div class="small-12 columns no-padding">
                                                <button data-type="city" data-action="delete" data-id="{{ $image->id }}" class="image-delete" style="width:100%;">
                                                    <i class="fa fa-times fa-sm white" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        <?php else: ?>            
                                            <div class="small-6 columns no-padding">
                                                <button data-type="city" data-action="update" data-id="{{ $image->id }}" class="image-primary image-click-event">
                                                    <i class="fa fa-user fa-sm white" aria-hidden="true"></i>
                                                </button>           
                                            </div>
                                            <div class="small-6 columns no-padding">
                                                <button data-type="city" data-action="delete" data-id="{{ $image->id }}" class="image-delete" style="width:100%;">
                                                    <i class="fa fa-times fa-sm white" aria-hidden="true"></i>
                                                </button>
                                            </div>                  
                                        <?php endif ?>

                                    </div>
                                </div>

                                <!-- display default blank image if file does not exist -->
                            <?php else: ?>

                                <a href="{{ url('uploads/no-image.png') }}" class="th">
                                    <?php if ($image->is_primary == TRUE || count($oCity->image) <= 1): ?>
                                        <span class="primary-image-container" data-id="{{ $image->id }}">
                                            <i class="fa fa-user fa-sm primary-image" aria-hidden="true"></i>
                                        </span>
                                    <?php endif ?>
                                    <img src="{{ url('uploads/no-image.png') }}">
                                </a>
                                <div class="" style="margin-top:4px;">
                                    <!-- check if image is the primary picture or if number of images returned is greater than 1-->
                                    <?php if ($image->is_primary == TRUE || count($oCity->image) <= 1): ?>
                                        <div class="small-12 columns no-padding">
                                            <button data-type="city" data-action="delete" data-id="{{ $image->id }}" class="image-delete" style="width:100%;">
                                                <i class="fa fa-times fa-sm white" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    <?php else: ?>
                                        <div class="small-6 columns no-padding">
                                            <button data-type="city" data-action="update" data-id="{{ $image->id }}" class="image-primary image-click-event">
                                                <i class="fa fa-user fa-sm white" aria-hidden="true"></i>
                                            </button>           
                                        </div>
                                        <div class="small-6 columns no-padding">
                                            <button data-type="city" data-action="delete" data-id="{{ $image->id }}" class="image-delete" style="width:100%;">
                                                <i class="fa fa-times fa-sm white" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    <?php endif ?>
                                </div>    

                            <?php endif ?>
                        </li>

                    <?php endforeach ?>

                    <!-- show the default blank image whenever there is no image uploaded for image for the city -->
                <?php else: ?>      

                    <li>
                        <img src="{{ url('uploads/no-image.png') }}">
                    </li>

                <?php endif; ?>
            </ul>
        </div>
    @endif

</div>
@stop

@section('custom-js')
<script>


$('#exampleCheckboxSwitch').bootstrapSwitch();
tinymce.init({
        selector:'#description',
        height: 250,
        menubar: false
});	

$( "#create" ).validate({
    rules: {
            name: "required",
            country_id:"required",
            default_nights:"required number",
            lat:"required",
            lng:"required",
        },
    messages: {
            name: "Name is required!",
            country_id: "Country is required!",
            default_nights: {
                required: "Default Nights is required!",
                number: "Please enter valid number!",
                },
            lat: "Ltitude is required!",
            lng: "Longitude is required!",
        }
});


@if($nIdCity != '')
    var countryCode = '{{ ($oCity) ? $oCity->country->code : "" }}';

    $('#aot-supplier-location-type').change(function(){
        getAotLocation();
    });
    
    $(document).ready(function(){
        buildAOTLocations();
        buildAERegions();
    });

var selectedAOTLocations = JSON.parse('{!! json_encode( $selected_aot_suppier_locations ) !!}');
if (selectedAOTLocations[0] == null){ selectedAOTLocations = []; }

$('#aot-supplier-locations').click(function(){
    var selected = [];
    $(this).find('option:selected').each(function(i, elem){ selected.push($(elem).data()); });
    selectedAOTLocations = selectedAOTLocations.filter(function(val){
        return val.LocationType != selectedAOTLocationType;
    });
    selected.forEach(function(s){
        selectedAOTLocations.push({
            LocationCode : s.locationCode,
            LocationName : s.locationName,
            LocationType : s.locationType
        });
    });
    buildSelectedAOTLocations();
});

function buildAOTLocations()
{
    $('#aot-supplier-locations').prop('disabled', true);
    if( countryCode == 'AU' )
    {
      $.ajax({ 
        url : '{{ route("common.aot-location") }}/'+$('#aot-supplier-location-type').val(),
        success : function( response ){
            debugger;
            var optionsHTML = '';
            response.forEach( function( l, lKey ){
                var isSelected   = '';
                selectedAOTLocations.forEach(function( sl ){
                    debugger;
                    if( sl.LocationCode == l.LocationCode ){ isSelected = 'selected="selected"'; }
                });
                var optionData = [
                  'data-location-code="'+l.LocationCode+'"',
                  'data-location-name="'+l.LocationName+'"',
                  'data-location-type="'+l.LocationType+'"'
                ].join(" ");
                optionsHTML += '<option value="'+l.LocationCode+'" '+optionData+' '+isSelected+'>'+l.LocationName+'</option>';
          });
          $('#aot-supplier-locations').html( optionsHTML ).prop('disabled', false);
          buildSelectedAOTLocations();
        }
      });
    }
    else
    {
      $('.aot-mapping-container').html('<div class="col-md-12 text-center">Not available.</div>');
    }
}  

function buildSelectedAOTLocations()
{
    var typeOrder = { S : 0, Z : 1, R : 2, T : 3, L : 4 };
    // sort the selectedAOTLocations by AOT LocationType order
    selectedAOTLocations.sort(function(a, b){
    return (typeOrder[ a.LocationType ] > typeOrder[ b.LocationType ]) ? 1 : - 1;
    });
    var selectedHTML = '';
    var typeName = { S : 'STATE', Z : 'SUPER REGION', R : 'REGION', T : 'TOWN', L : 'LOCATION' };
    var selectedAOTLocationCodes = [];
    selectedAOTLocations.forEach(function(sl){

    selectedAOTLocationCodes.push(sl.LocationCode);
    selectedHTML += [
            '<item class="selected-aot-locations" data-location-code="' + sl.LocationCode + '">',
            typeName[ sl.LocationType ] + ' - ' + sl.LocationName,
            '</item>',
            '<remove data-provider="aot" data-location-code="' + sl.LocationCode + '">',
            '<i class="fa fa-close fa-1x"></i>',
            '</remove><br/>'
    ].join("");
    });
    $('input[name="aot_location_codes"]').val(JSON.stringify(selectedAOTLocationCodes));
    $('#selected-aot-locations').html(selectedHTML);
}
 
var selectedAERegions = JSON.parse('{!! json_encode( $aAeCityMapSelected ) !!}');
if (selectedAERegions[0] == null){ selectedAERegions = []; }

$('#ae-regions').click(function(){
    var selected = [];
    $(this).find('option:selected').each(function(i, elem){ selected.push($(elem).data()); });
    selectedAERegions = [];
    selected.forEach(function(s){
        selectedAERegions.push({
            RegionId : s.regionId,
            Name : s.name
        });
    });
    buildSelectedAERegions();
});
    
function buildAERegions()
{   
    //var oRegion = selectedAERegions;
    var oRegion = JSON.parse('{!! $oRegion !!}');

    var optionsHTML = '';
    var isSelected = '';
    for (var x in oRegion){
        
        var isSelected = '';
        selectedAERegions.forEach(function(sr){
            if (sr.id == oRegion[x].id){ isSelected = 'selected="selected"'; }
        });
        var optionData = [
            'data-region-id="' + oRegion[x].id + '"',
            'data-name="' + oRegion[x].Name + '"'
            ].join(" ");
        optionsHTML += '<option ' + optionData + ' ' + isSelected + '>' + oRegion[x].Name + '</option>';
    }
    $('#ae-regions').html(optionsHTML);
    buildSelectedAERegions();
}

function buildSelectedAERegions()
{
    selectedAERegions.sort(function(a, b){
    return (a.Name > b.Name) ? 1 : - 1;
    });
    var selectedHTML = '';
    var selectedAERegionIds = [];
    selectedAERegions.forEach(function(sr){

    selectedAERegionIds.push(sr.id);

    selectedHTML += [
            '<item class="selected-hb-destinations" data-region-id="' + sr.RegionId + '">',
            sr.Name,
            '</item>',
            '<remove data-provider="ae" data-region-id="' + sr.RegionId + '">',
            '<i class="fa fa-close fa-1x"></i>',
            '</remove><br/>'
    ].join("");
    });
    $('input[name="ae_region_ids"]').val(JSON.stringify(selectedAERegionIds));
    $('#selected-ae-regions').html(selectedHTML);
}

$('input[type="file"]').change(function(e){
    var fileName = e.target.files[0].name;
    $('.input-filename').html(fileName);
    $('.input-filename').show();
    $('#image-submit-text').hide();
    $('#image-submit-load').show();
    var formData = new FormData($('form#image-form')[0]);
    $.ajax({
        url:'{{ route("common.city-images") }}',
        type:  'POST',
        data:  formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.success === true){
            $('.image-alert-message').html(data.data.message);
            $('ul.clearing-thumbs').append(addImageToImageList(data.data[0]));
            var inputImageFile = $("#image");
            inputImageFile.replaceWith(inputImageFile = inputImageFile.clone(true));
            } else{
            $('.image-alert-message').html(data.error.message);
            }
        },
        error: function()
        {
            $('.image-alert-message').html('An error has occured. Invalid file.');
        },
        complete: function()
        {
            $('.input-filename').fadeOut({
            duration:400,
                    complete:function(){
                    $('.input-filename').empty();
                    }
            });
            $('#image-submit-load').hide();
            $('#image-submit-text').show();
            $('.image-alert-box').slideDown(200);
        }
    });
});
function addImageToImageList(newImage)
{
    var html = '';
    html = '<li data-id="' + newImage.id + '">' +
            '<a href="' + siteUrl(newImage.large) + '" class="th">';
    if (newImage.is_primary){
    html += '<span class="primary-image-container" data-id="' + newImage.id + '">' +
            '<i class="fa fa-user fa-sm primary-image" aria-hidden="true"></i>' +
            '</span>';
    }
    html += '<img src="' + siteUrl(newImage.thumbnail) + '">' +
            '</a>' +
            '<div style="padding:2px;"><div class="row" style="margin-top:4px;">';
    if (newImage.is_primary){
    html += '<div class="small-12 columns no-padding">' +
            '<button data-type="city" data-action="delete" data-id="' + newImage.id + '" class="image-delete"   style="width:100%;">' +
            '<i class="fa fa-times fa-sm white" aria-hidden="true"></i>' +
            '</button>' +
            '</div>';
    } else{
    html += '<div class="small-6 columns no-padding">' +
            '<button data-type="city" data-action="update" data-id="' + newImage.id + '" class="image-primary image-click-event">' +
            '<i class="fa fa-user fa-sm white" aria-hidden="true"></i>' +
            '</button>' +
            '</div>' +
            '<div class="small-6 columns no-padding">' +
            '<button data-type="city" data-action="delete" data-id="' + newImage.id + '" class="image-delete" style="width:100%;">' +
            '<i class="fa fa-times fa-sm white" aria-hidden="true"></i>' +
            '</button>' +
            '</div>';
    }
    html += '</div></div>' +
            '</li>';
    return html;
}

@endif
</script>
@stop
