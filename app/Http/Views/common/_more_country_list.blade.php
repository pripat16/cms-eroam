@foreach ($oCountryList as $aCountry)
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aCountry->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aCountry->id;?>" value="<?php echo $aCountry->id;?>">&nbsp;
            </label>
        </td>
        <td>
            <a href="#">
                {{ $aCountry->code.'-'.$aCountry->name }}
            </a>
        </td>
        <td>{{ $aCountry->region_name }}</td>
        <td class="text-center">
            <div class="switch tiny switch_cls">
                <input class="show-on-eroam-btn switch1-state1" data-id="{{ $aCountry->id }}" id="show-on-eroam-{{ $aCountry->id }}" type="checkbox" {{ $aCountry->show_on_eroam == 1 ? 'checked' : '' }}>
                <label for="show-on-eroam-{{ $aCountry->id }}"></label>
            </div>
        </td>
    </tr> 
@endforeach