@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">
    <h1 class="page-title">{{ trans('messages.add_currency') }}</h1>
    <div class="row">
        @if (Session::has('message'))
        <div class="small-6 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif

    </div>
    <br>
    @if(isset($oCurrency))
    {{ Form::model($oCurrency, array('url' => route('common.create-currency') ,'method'=>'POST','enctype'=>'multipart/form-data','id'=>'addTourForm')) }}
    @else
    <form method="POST" action="{{ route('common.create-currency') }}"> 
    @endif

    <div class="box-wrapper">

        <p>{{ trans('messages.currency_detail') }}</p>
        {{ csrf_field() }}

        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.name') }} <span class="required">*</span></label>
            <?php
            $attributes = 'form-control';
            ?>
            {{Form::text('name',Input::old('name'),['id'=>'name','class'=>$attributes,'placeholder'=>'Enter Name'])}}

        </div>
        @if ( $errors->first( 'name' ) )
        <small class="error">{{ $errors->first('name') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">Code <span class="required">*</span></label>
            <?php
            $attributes = 'form-control';
            ?>
            {{Form::text('code',Input::old('code'),['id'=>'code','class'=>$attributes,'placeholder'=>'Enter Code'])}}

        </div>
        @if ( $errors->first( 'code' ) )
        <small class="error">{{ $errors->first('code') }}</small>
        @endif
    </div>
    <input type="hidden" name="currency_id" value="{{ $nIdCurrency }}" />

    <div class="m-t-20 row col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-sm-6">
                {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
            </div>
            <div class="col-sm-6">
                <a href="{{ route('common.currency-list') }}" class="btn btn-primary btn-block">Cancel</a>
            </div>
        </div>
    </div>	
    {{Form::close()}}
</div>
@stop

@section('custom-css')
<style>

    .error{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .error_message{
        color:red !important;
    }

    .success_message{
        color:green !important;
        text-align: center;
    }
</style>
@stop

@section('custom-js')
<script>


</script>
@stop
