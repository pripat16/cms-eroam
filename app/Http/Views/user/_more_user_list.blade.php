@foreach ($oUserList as $aUser)				
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aUser->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aUser->id;?>" value="<?php echo $aUser->id;?>">&nbsp;
            </label>
        </td>
        <td>
            <a href="#">
                {{ $aUser->name }}
            </a>
        </td>
        <td>{{ $aUser->username }}</td>
        <td class="text-center"><i class="fa fa-{{ $aUser->active == 1 ? 'check' : 'times' }}"></i></td>	
    </tr> 
@endforeach