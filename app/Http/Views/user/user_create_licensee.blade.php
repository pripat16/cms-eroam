@extends( 'layout/mainlayout' )

@section('content')
    <div class="content-container">
        <h1 class="page-title">{{ trans('messages.add_new_licensee') }}</h1>
        <div class="row">
            @if ( Session::has( 'message' ) && Session::get( 'message' ) == 'success' )
                <div class="small-6 small-centered columns success-box">
                    <a href="{{ URL::to( 'licensee/' . Session::get( 'user_id' ) ) }}">{{ Session::get( 'licensee_name' ) }}</a> 
                    account has been created.
                </div>
            @endif

        </div>	
        <br>
        <form action="{{ route('user.create-licensee') }}" method="post">
            {{ csrf_field() }}
            <div class="box-wrapper">
                <p>{{ trans('messages.licensee_detail') }}</p>
                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.licensee_name') }}<span class="required">*</span></label>
                    <input type="text" id="name" class="form-control" placeholder="{{ trans('messages.licensee_name_placeholder') }}" name="name" required value="{{ old('name') }}"/>
                </div>
                @if ( $errors->first( 'name' ) )
                    <small class="error">{{ $errors->first('name') }}</small>
                @endif
                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.licensee_domain') }} <span class="required">*</span></label>
                    <input type="text" id="domain" class="form-control" placeholder="{{ trans('messages.licensee_domain_placeholder') }}" name="domain" required value="{{ old('domain') }}" />
                </div>
                @if ( $errors->first( 'domain' ) )
                    <small class="error">{{ $errors->first('domain') }}</small>
                @endif
                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.licensee_email') }} <span class="required">*</span></label>
                    <input type="text" id="email" class="form-control" placeholder="{{ trans('messages.licensee_email_placeholder') }}" name="username" required value="{{ old('username') }}" />
                </div>
                @if ( $errors->first( 'username' ) )
                    <small class="error">{{ $errors->first('username') }}</small>
                @endif

                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.licensee_password') }} <span class="required">*</span></label>
                    <div class="small-2 column">
                        <a class="prefix toggle-password button secondary tiny btn-sm btn-primary">{{ trans('messages.hide') }}</a> 
                        <input name="password" type="text" value="" required >
                        <a href="#" class="button postfix generate-password btn-sm btn-primary">{{ trans('messages.generate') }}</a>
                    </div>
                </div>
                @if ( $errors->first( 'password' ) )
                    <small class="error">{{ $errors->first('password') }}</small>
                @endif
                
                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.licensee_duration') }} <span class="required">*</span></label>
                    <input min="0" id="duration" class="form-control" placeholder="{{ trans('messages.licensee_duration_placeholder') }}" name="duration" type="number" value="7" required value="{{ old('duration') }}">

                </div>
                @if ( $errors->first( 'duration' ) )
                    <small class="error">{{ $errors->first('duration') }}</small>
                @endif
                
                <div class="row">
                    <div class="m-t-20 row col-md-8 col-md-offset-2">
                        <div class="row">
                            <div class="col-sm-6">
                                <input class="button success btn btn-primary btn-block" type="submit" value="{{ trans('messages.create_account_btn') }}">
                            </div>
                            <div class="col-sm-6">
                                <a href="{{ route('user.list',['sUserType' => 'licensee' ]) }}" class="btn btn-primary btn-block">{{ trans('messages.cancel_btn') }}</a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('custom-css')
<style type="text/css">
	.error{
			color:red !important;
	}
	.success-msg {
		background: #67BB67;
		color: #fff;
		padding: 5px;
	}
	.success-msg a {
		color: #fff;
		text-decoration: underline;
	}
	.error_message{
		color:red !important;
	}
	.with_error{
		border-color: red !important;
	}
	.success_message{
		color:green !important;
		text-align: center;
	}
	div .with_error{
		border:1px solid black;
	}
</style>
@stop

@section('custom-js')
<script type="text/javascript">
    $( function() {
        $( '.generate-password' ).click( function( e ) {
            e.preventDefault();
            var generatedHash = Math.random().toString(36).slice(-16).toUpperCase();
            $( 'input[name="password"]' ).val( generatedHash );
        });

        $( '.toggle-password' ).click( function( e ) {
            password = $( 'input[name="password"]' );
            if ( password.attr( 'type' ) == 'text' ) {
                   password.attr( 'type', 'password' );
                   $( this ).text( 'Show' );
            } else {
                   password.attr( 'type', 'text' );
                   $( this ).text( 'Hide' );
            }
        });
    });
</script>
@stop