@extends( 'layout/mainlayout' )

@section('content')
 <div class="content-container">
    <h1 class="page-title">{{ trans('messages.manage_tour_type_logo') }}</h1>
    @if(Session::has('message'))
    <div class="row">
        <div class="small-6 small-centered columns success-box">{{ Session::get('message') }}</div>
    </div> 
    <br>
    @endif
    <div class="box-wrapper">
        <a href="{{ route('tour.tour-logo-create') }}" class="plus-icon" title="Add"><i class="icon-plus"></i></a>
        <p> 
            <?php
            if (count($oTourTypeLogoList) > 1) {
                echo 'Tour Type Logos';
            } else {
                echo 'Tour Type Logo';
            }
            ?>
        </p>
        <div class="row m-t-20 search-wrapper">
            <div class="col-md-7 col-sm-7">
                <div class="input-group input-group-box">
                    <input type="text" class="form-control" placeholder="Search Title" name="search_str" value="{{ $sSearchStr }}">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" onclick="callTourLogoListing(event,'table_record')"><i class="icon-search-domain"></i></button>
                    </span>
                </div>
            </div>
        </div>
        <div class="table-responsive m-t-20 table_record">
        @include('WebView::tour._more_tour_logo_list')
        </div>
    </div>
 </div>	
 @stop

 @section('custom-js')
 <script>
function getTourSort(element,sOrderField)
{
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('desc');
        callTourLogoListing(element,'table_record');
    }
    else
    {
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('asc');
        callTourLogoListing(element,'table_record');
    }
}
$(document).on('click',".cmp_check",function () { 

    if($('.cmp_check:checked').length == $('.cmp_check').length){
        $('#checkbox-00').prop('checked',true);
    }else{
        $('#checkbox-00').prop('checked',false);
    }
});
$(document).on('click','.label_check',function(){
    setupLabel();
});  
         $('.alert1').click(function(){
             var c = confirm("Are you sure you want to delete this record?");
             return c;	
         });

   $(document).ready(function () {
     var cmp_tour_multiple = [] ;
    
     $("#checkbox-tours").click(function () { 
         var cmp_tour = [] ;
         if(this.checked) {
             // Iterate each checkbox
             $('.cmp_tour_check').each(function() {
                     this.checked = true;
                     cmp_tour.push($(this).val());
                     cmp_tour_multiple.push($(this).val());
                 });
                 //$('#dropdownMenu1').prop('disabled', false);
             } else {
                 $('.cmp_tour_check').each(function () {
                     this.checked = false;
                 });
                 cmp_tour_multiple = [];
                 //$('#dropdownMenu1').prop('disabled', true);
             }
         });
     });

 </script>
 @stop
 @section('custom-css')
 <style>
     .success_message{
         color:green !important;
         text-align: center;
     }
 </style>
 @stop