    <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
    <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
    <table class="table">
        <thead>
            <tr>
                <th>
                    <label class="radio-checkbox label_check" for="checkbox-00">
                        <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                    </label>
                </th>
                <th onclick="getTourSort(this, 'title');">{{ trans('messages.logo_title') }} 
                    <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'title')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
                </th>
                <th>{{ trans('messages.logo_image') }}</th>
                <th class="">{{ trans('messages.action_head') }}</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (count($oTourTypeLogoList) > 0) {
                foreach ($oTourTypeLogoList as $TourTypeLogo):
                    ?>
                    <tr>
                        <td>
                            <label class="radio-checkbox label_check" for="checkbox-{{ $TourTypeLogo->id }}">
                                <input type="checkbox" class="cmp_check" id="checkbox-{{ $TourTypeLogo->id }}" value="{{ $TourTypeLogo->id }}">&nbsp;
                            </label>
                        </td>
                        <td><a href="{{ route('tour.tour-logo-create',[ 'nIdTourLogo' => $TourTypeLogo->id]) }}">{{$TourTypeLogo->title}}</a></td>
                        <td><img src="{{ url($TourTypeLogo->logo_path) }}" class="img-responsive"></td>
                        <td>
                            <a href="{{ route('tour.tour-logo-create',[ 'nIdTourLogo' => $TourTypeLogo->id]) }}" class="button success tiny btn-primary btn-sm pull-left m-r-10">{{ trans('messages.update_btn') }}</a>
                            <a href="{{ route('tour.tour-logo-delete',[ 'nIdTourLogo' => $TourTypeLogo->id]) }}" class="button success tiny btn-primary btn-sm pull-left m-r-10">{{ trans('messages.delete_btn') }}</a>
                        </td>	
                    </tr>
                <?php endforeach; ?>
            <?php }else {
                ?>
                <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found')}}</td></tr>
                <?php
            }
            ?>
        </tbody>
    </table>

