@foreach ($oActivitySeasonList as $key => $aActivity)				
    <tr class="clickable <?php echo (($key == 0) ? 'open':'')?>" data-toggle="collapse" id="row-{{$aActivity->id}}" data-target=".row-{{$aActivity->id}}">
        <td><i class="icon-unfold-less"></i></td>
        <td>
            <a href="#">
                {{ $aActivity->name }}
            </a>    
        </td>
        <td>{{ $aActivity->city_name }}</td>
        <td>{{ $aActivity->operator_name }}</td>
        <td>-</td>  
        <td>-</td>
        <td>-</td>
        <td>-</td>  
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td class="text-center">-</td>
    </tr>
    @foreach($aActivity->price as $season)
    <tr class="collapse row-{{$aActivity->id}}">
        <td>
            <label class="radio-checkbox label_check" for="checkbox-{{$season->id}}">
                <input type="checkbox" id="checkbox-{{$season->id}}" value="{{$season->id}}">&nbsp;
            </label>
        </td>
        <td>-</td>  
        <td>-</td>
        <td>-</td>
        <td><a href="{{URL::to('activity/season/'.$season->id)}}">{{$season->name}}</a></td>
        <td>{{date('d/m/y',strtotime($season->date_to))}}</td>
        <td>{{date('d/m/y',strtotime($season->date_from))}}</td>
        <td>{{$season->minimum_pax}}</td>
        <td>{{$season->allotment}}</td>
        <td>
            {{$season->currency->code}} {{number_format(round($season->price, 2), 2, '.', ',')}}
        </td>
        <td>
            <?php if ($season->supplier): ?>
                {{ $season->supplier->name }}	
            <?php endif ?>
        </td>
        <td>
            <a href="{{ route('activity.activity-season-create',['nSeasonId'=>$season->id])}}" class="button success tiny btn-primary btn-sm pull-left m-r-10" style="margin-bottom:2px;">Update</a>
            {{Form::open(array('url' => 'activity/season/'.$season->id,'method'=>'Delete')) }}{{Form::submit('Delete',['class'=>'button alert1 tiny btn-primary btn-sm','style'=>'margin-bottom:2px;']) }}{{Form::close()}}
        </td>
    </tr>
    @endforeach
        
    </tr> 
@endforeach