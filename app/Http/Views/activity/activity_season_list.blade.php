@extends( 'layout/mainlayout' )

@section('custom-css')
<style type="text/css">
.select-user-type {
	display: inline-block;
	border-radius: 4px;
	text-align: center;
	font-size: 0.9rem;
	background: #dcdcdc;
	padding: 10px 25px;
	color: #333;
	transition: all .2s;
}
.select-user-type:hover, .select-user-type.selected {
	background: #666666;
	color: #fff;
}
.select-user-type.selected {
	cursor: default;
}
.search-box {
	margin: 25px 0;
	position: relative;
}
.search-box i.fa {
	position: absolute;
	top: 10px;
	left: 7px;
}
#search-key {
	padding-left: 25px;
}
.fa-check {
	color: #1c812f;
}
.fa-times,
.fa-exclamation-circle {
	color: #bd1b1b;
}
.user-name a {
	color: #2b78b0;
	font-weight: bold;
}
.ajax-loader {
	font-size: 1.5rem;
	display: none;
}
</style>
@stop

@section('content')

<div class="content-container">
    <h1 class="page-title">Manage {{ trans('messages.activity') }}</h1>

    @if(Session::has('message'))
        <div class="small-6 small-centered columns success_message">{{ Session::get('message') }}</div>
        <br>
    @endif
    <div class="box-wrapper">
        <a href="{{ route('activity.activity-season-create') }}" class="plus-icon" title="Add">
            <i class="icon-plus"></i>
        </a>
        <p>{{ $oActivitySeasonList->count().' '. trans('messages.activity')  }}</p>
        <div class="row m-t-20 search-wrapper">
            <div class="col-md-5 col-sm-5">
	          <div class="form-group">
	          	<input type="text" name="search_str" value="{{Input::get('search')}}" class="form-control m-t-10" placeholder="Search Season"> 
	          </div>
	        </div>
	        
	        <div class="col-md-5 col-sm-5">
                    <div class="form-group">
                        <select name="search-by" class="form-control m-t-10 search_by">
                            <option value="a.name" {{(Input::get('search-by') == 'activity') ? 'selected' :''}}>
                                Activity name
                            </option>
                            <option value="c.name" {{(Input::get('search-by') == 'c.name') ? 'selected' :''}}>
                                City
                            </option>
                            <option value="ao.name" {{(Input::get('search-by') == 'ao.name') ? 'selected' :''}}>
                                Operator
                            </option>									
                            <option value="name" {{(Input::get('search-by') == 'name') ? 'selected' :''}}>
                                Season name
                            </option>							
                            <option value="price" {{(Input::get('search-by') == 'price') ? 'selected' :''}}>
                                Price
                            </option>
                            <option value="pax" {{(Input::get('search-by') == 'pax') ? 'selected' :''}}>
                                Pax
                            </option>
                            <option value="allotment" {{(Input::get('search-by') == 'allotment') ? 'selected' :''}}>
                                Allotment
                            </option>
                            <option value="supplier" {{(Input::get('search-by') == 'supplier') ? 'selected' :''}}>
                                Supplier
                            </option>	
                        </select>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2">
                    <input type="submit" class="button success tiny btn-primary btn-md" id="search" value="Search" onclick="getMoreListing(siteUrl('activity/activity-season-list'),event,'table_record');">
                </div>
            <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
            <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
            
        </div>
        <div class="m-t-30">
            <label>{{ trans('messages.show_record') }}</label>
            <select class="select-entry" name="show_record" onchange="getMoreListing(siteUrl('activity/activity-season-list'),event,'table_record');">
                <option value="10" {{ ($nShowRecord == 10) ? 'selected="selected"' : '' }}>10</option>
                <option value="20" {{ ($nShowRecord == 20) ? 'selected="selected"' : '' }}>20</option>
                <option value="30" {{ ($nShowRecord == 30) ? 'selected="selected"' : '' }}>30</option>
                <option value="40" {{ ($nShowRecord == 40) ? 'selected="selected"' : '' }}>40</option>
            </select>
            <label>{{ trans('messages.entries') }}</label>
        </div>
        <div class="table-responsive m-t-20 table_record">
           
            @include('WebView::activity._activity_season_list_ajax')
      
        </div>
    </div>

</div>
@stop
@section('custom-js')
<script type="text/javascript">

function getActivitySort(element,sOrderField)
{
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('desc');
        getMoreListing(siteUrl('activity/activity-season-list'),event,'table_record');
    }
    else
    {
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('asc');
        getMoreListing(siteUrl('activity/activity-season-list'),event,'table_record');
    }
}

$(document).on('click',".cmp_check",function () { 

    if($('.cmp_check:checked').length == $('.cmp_check').length){
        $('#checkbox-00').prop('checked',true);
    }else{
        $('#checkbox-00').prop('checked',false);
    }
});
$(document).on('click','.label_check',function(){
    setupLabel();
});  
</script>
@stop