@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">
    <h1 class="page-title">{{ trans('messages.add_new_season') }}</h1>
    <div class="row">
        @if (Session::has('message'))
        <div class="small-6 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif
        @if ($errors->any())
        <div class="small-6 small-centered columns error-box">{{$errors->first()}}</div>
        @endif
    </div>
    <br>
    <?php $sDisable = ''; ?>
    @if(isset($oSeason))
    <?php $sDisable = 'readonly'; ?>
   {{ Form::model($oSeason, array('url' => route('activity.activity-season-create') ,'method'=>'POST','enctype'=>'multipart/form-data')) }}
   @else
   {{Form::open(array('url' => route('activity.activity-season-create'),'method'=>'Post','enctype'=>'multipart/form-data')) }}
   @endif
    <div class="box-wrapper">

        <p>{{ trans('messages.season_details') }}</p>
        

        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.name') }} <span class="required">*</span></label>
            {{Form::text('name',Input::old('name'),['id'=>'name','class'=>'form-control','placeholder'=>'Enter Name', $sDisable])}}

        </div>
        @if ( $errors->first( 'name' ) )
        <small class="error">{{ $errors->first('name') }}</small>
        @endif
        @if ($nFromFlag == '')	
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.activity') }} <span class="required">*</span></label>
            {{ Form::select('activity_id',$activities,Input::old('activity_id'),['class'=>'form-control', $sDisable])}}	

        </div>
        @endif	
        @if ( $errors->first( 'activity_id' ) )
        <small class="error">{{ $errors->first('activity_id') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.date_from') }} <span class="required">*</span></label>
            {{Form::input('text','date_from',Input::old('date_from'),['id'=>'date_from','class' =>'form-control','placeholder'=>'Enter Date From',$sDisable])}}	

        </div>
        @if ( $errors->first( 'date_from' ) )
        <small class="error">{{ $errors->first('date_from') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.date_to') }} <span class="required">*</span></label>
            {{Form::input('text','date_to',Input::old('date_to'),['id'=>'date_to','class' =>'form-control','placeholder'=>'Enter Date To',$sDisable])}}

        </div>
        @if ( $errors->first( 'date_to' ) )
        <small class="error">{{ $errors->first('date_to') }}</small>
        @endif
    </div>
    <div class="box-wrapper"> 
        <p>{{ trans('messages.price_package_detail') }}</p>  
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.price') }}</label>
            <?php $price = isset($oSeason) ? $oSeason->base_price->base_price : Input::old('price') ; ?>
            {{Form::number('price',$price,['id'=>'price','step'=>"any",'min'=>'1','class'=>'form-control','placeholder'=>'Enter Price'])}}

        </div>

        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.allotment') }} <span class="required">*</span></label>
            {{Form::number('allotment',Input::old('allotment'),['id'=>'allotment','min'=>'1','class'=>'form-control','placeholder'=>'Enter Allotment'])}}
        </div>
        @if ( $errors->first( 'allotment' ) )
        <small class="error">{{ $errors->first('allotment') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.operates') }} <span class="required">*</span></label>
            {{Form::select('operates[]',$operates,null,array('multiple'=>'true','class'=>'form-control'))}}
        </div>
        @if ( $errors->first( 'operates' ) )
        <small class="error">{{ $errors->first('operates') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.min_pax') }} <span class="required">*</span></label>
            {{Form::number('minimum_pax',Input::old('minimum_pax'),['id'=>'minimum_pax','min'=>'1','class'=>'form-control','placeholder'=>'Enter Minimum Pax'])}}
        </div>
        @if ( $errors->first( 'minimum_pax' ) )
        <small class="error">{{ $errors->first('minimum_pax') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.release') }} <span class="required">*</span></label>
            {{Form::number('release',Input::old('release'),['id'=>'release','min'=>'1','class'=>'form-control','placeholder'=>'Enter Release'])}}		
        </div>
        @if ( $errors->first( 'release' ) )
        <small class="error">{{ $errors->first('release') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.currency_title') }} <span class="required">*</span></label>
            {{Form::select('currency_id',$currencies,Input::old('currency_id'),['id'=>'currency_id','class'=>'form-control'])}}
        </div>
        @if ( $errors->first( 'currency_id' ) )
        <small class="error">{{ $errors->first('currency_id') }}</small>
        @endif
    </div>
    <div class="box-wrapper">
        <p>{{ trans('messages.cancellation_policy') }}</p>

        <div class="form-group m-t-30">
            {{Form::textarea('cancellation_policy',Input::old('cancellation_policy'),['placeholder'=>'Description','id'=>'cancellation_policy','class'=>'form-control cancellation_policy'])}}
        </div> 
    </div>  
    <div class="box-wrapper">
        <p>{{ trans('messages.cancellation_policy_percent') }}</p>
        <div class="row tb_added">
            @if(Input::old('days'))
                @foreach(Input::old('days') as $key => $day)
                <div class="col-sm-6">
                    <div class="form-group m-t-30">
                        <label class="label-control">{{ trans('messages.day_before') }}</label>

                        <input placeholder="Enter Days Before" type="number" class="form-control" name="days[]" min="1" id="days" value="{{$day}}">
                    </div> 
                </div> 
                <div class="col-sm-6">
                    <div class="form-group m-t-30">
                        <label class="label-control">{{ trans('messages.percentage') }}</label>

                        <input placeholder="Enter Percentage" type="number" class="form-control" name="percent[]" min="1" id="percent" value="{{Input::old('percent')[$key]}}">

                    </div> 
                </div> 
                @endforeach
            @elseif(isset($decoded_formula))
                @foreach ($decoded_formula as $key => $formula)
                <div class="row tb_added">
                    <div class="col-sm-6">
                        <div class="form-group m-t-30">
                            <label class="label-control">{{ trans('messages.day_before') }}</label>
                            <input type="number" class="form-control" name="days[]" min="1" id="days" value="{{$formula->days}}" placeholder="Enter Days Before">
                        </div> 
                    </div> 
                    <div class="col-sm-6">
                        <div class="form-group m-t-30">
                            <label class="label-control">{{ trans('messages.percentage') }}</label>
                            <input type="number" class="form-control" name="percent[]" min="1" id="percent" value="{{$formula->percent}}" placeholder="Enter Percentage">
                        </div> 
                    </div> 
                </div>
                @endforeach
            @else
                <div class="col-sm-6">
                    <div class="form-group m-t-30">
                        <label class="label-control">{{ trans('messages.day_before') }}</label>
                        {{Form::number('days[]',Input::old('days.0'),['id'=>'days','min'=>'1','class'=>'form-control','placeholder'=>'Enter Days Before'])}}
                    </div> 
                </div> 
                <div class="col-sm-6">
                    <div class="form-group m-t-30">
                        <label class="label-control">{{ trans('messages.percentage') }}</label>
                        {{Form::number('percent[]',Input::old('percent.0'),['id'=>'percent','min'=>'1','class'=>'form-control','placeholder'=>'Enter Percentage'])}}

                    </div> 
                </div> 
            @endif

        </div> 
        <div class="clones"></div>
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    <i class="fa fa-minus-square right fa-2x" id="fa-minus" aria-hidden="true"></i>
                    <i class="fa fa-plus-square right fa-2x" id="fa-plus" aria-hidden="true"></i>
                </div> 
            </div>      
        </div>     
    </div>  

    <div class="m-t-20 row col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-sm-6">
                {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
            </div>
            <div class="col-sm-6">
                <a href="{{URL::to('activity/activity-season-list')}}" class="btn btn-primary btn-block">{{ trans('messages.cancel_btn') }}</a>
            </div>
        </div>
    </div>
    {{ Form::hidden('id', $nId) }}
    {{ Form::hidden('from_flag', $nFromFlag) }}
    {{Form::close()}}
</div>
@stop

@section('custom-css')
<style>
    .error{
        color:red !important;
    }
    .error_message{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    div .with_error{
        border:1px solid black;
    }
    label {
        font-weight: 700;
    }
    .preview-image {
        width: 40%;
        margin-bottom: 25px;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }
</style>
@stop

@section('custom-js')
<script>
    $(function () {

        $("#date_to,#date_from").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true
        });

    })

    tinymce.init({
        selector: '#cancellation_policy',
        height: 250,
        menubar: false
    });
    $('#fa-plus').click(function () {
//        debugger;
        var count = $('.clones').children().length+1;
        $('.tb_added:last').clone().appendTo(".clones");
//        var element =  $('.clones .tb_added:last-child');
//        element.find('.form-control:first').attr('name','days['+ count +']');
//        element.find('.form-control:last').attr('name','percent['+ count +']');
        var numItems = $('.tb_added').length;

    });
    $('#fa-minus').click(function () {
        var numItems = $('.tb_added').length;
        if (numItems > 1) {
            $('.tb_added:last').remove();
        }
    });
</script>
@stop