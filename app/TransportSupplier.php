<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransportSupplier extends Model
{
    protected $fillable = [
                        'name', 'logo', 'description','marketing_contact_name','marketing_contact_email','marketing_contact_title','marketing_contact_phone',
                        'reservation_contact_name', 'reservation_contact_email', 'reservation_contact_landline','reservation_contact_free_phone',
                        'accounts_contact_name','accounts_contact_email','accounts_contact_title','accounts_contact_phone','special_notes','remarks'
                        ];
    protected $table = 'ztransportsuppliers';
    protected $primaryKey = 'id';
    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
