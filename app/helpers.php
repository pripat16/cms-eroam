<?php

if (!function_exists('image_validator'))
{	
    function image_validator($image){
        $fails   = TRUE;
        $message = '';

        if( $image->isValid() ){
            return 0;
            $ext = strtolower($image->getClientOriginalExtension());


            $extension = ['jpeg','jpg','png','bmp', 'JPG'];
            if(in_array($ext,$extension)){
                    if($image->getSize() < 25000000){
                            $fails = FALSE;
                    }else{
                            $message = 'File size should not exceed 25MB.';
                    }	
            }else{
                    $message = 'File uploaded is not an image.';
            }
        }else{
                $message = 'File uploaded is not valid.';
        }
        return array('fails' => $fails, 'message' => $message);
    }
}
if (!function_exists('resize_image_to_thumbnail'))
{
    function resize_image_to_thumbnail($img)
    {
        // declare width and height variables
        $width  = $img->width();
        $height = $img->height();

        // if image width and heigth is equal to or greater than 150 and is equal in length 
        if( $width >= 150 && $height >= 150  && $width == $height){
                // resize to 150
                $img->resize(150, 150, function ($constraint) {
                $constraint->aspectRatio();
                });		

        // if image width and heigth is equal to or greater than 150 and width is greater than height
        }elseif( $width >= 150 && $height >= 150  && $width > $height){
                // resize to 150
                $img->resize(null, 150, function ($constraint) {
                $constraint->aspectRatio();
                });	
                // crop to 150x150
                $img->crop(150, 150);

        // if image width and heigth is equal to or greater than 150 and width is lesser than height
        }elseif( $width >= 150 && $height >= 150  && $width < $height){
                // resize to 150
                $img->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
                });	
                // crop to 150x150
                $img->crop(150, 150);

        // if only image width is equal to or greater than 150
        }elseif( $width >= 150 && $height < 150 ){
                // resize width to 150; height maybe shorter
                $img->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
                });	

        // if only image height is equal to or greater than 150
        }elseif( $width < 150 && $height >= 150 ){
                // resize height to 150; width maybe shorter
                $img->resize(null, 150, function ($constraint) {
                $constraint->aspectRatio();
                });	
        }

        // if none of the conditions are met (width and height are lesser than 150), no changes to the image will be made;
        return $img;
    }
}

if (!function_exists('resize_image_to_small'))
{
    function resize_image_to_small($img)
    {	
        // declare width and height variables
        $width  = $img->width();
        $height = $img->height();

        // height is greater than 300; 
        if( $height > 300){
                // resize to 300
                $img->resize(null, 300, function ($constraint) {
                $constraint->aspectRatio();
                });	
        }

        return $img;
    }	
}

if (!function_exists('resize_image_to_medium'))
{
    function resize_image_to_medium($img)
    {
        // declare width and height variables
        $width  = $img->width();
        $height = $img->height();

        // width is greater than 768; 
        if( $width > 768){
                // resize to 300
                $img->resize(768, null, function ($constraint) {
                $constraint->aspectRatio();
                });	
        }

        return $img;
    }	
}

if (!function_exists('resize_image_to_large'))
{
    function resize_image_to_large($img){	

        // declare width and height variables
        $width  = $img->width();
        $height = $img->height();

        // height is greater than 1024; 
        if( $height > 1024){
                // resize to 300
                $img->resize(null, 1024, function ($constraint) {
                $constraint->aspectRatio();
                });	
        }

        return $img;
    }	
}

if (!function_exists('response_format')){

	function response_format($success, $data, $error){
		if(is_array($data)){
			if(count($data) < 1)
				$data = NULL;
		}
		if(is_array($error)){
			if(count($error) < 1)
				$error = NULL;
		}		
		$result = array(
			'success' => $success,
			'data' => $data,
			'error' => $error
		);
		return Response::json( $result );
	}

}
if (!function_exists('resize_image_for_tour')){
    function resize_image_for_tour($img) {

        // declare width and height variables
        $width = $img->width();
        $height = $img->height();

        // if image width and heigth is equal to or greater than 150 and is equal in length 
        if ($width >= 245 && $height >= 169 && $width == $height) {
            // resize to 150
            $img->resize(245, 169, function ($constraint) {
                $constraint->aspectRatio();
            });

            // if image width and heigth is equal to or greater than 150 and width is greater than height
        } elseif ($width >= 245 && $height >= 169 && $width > $height) {
            // resize to 150
            $img->resize(null, 169, function ($constraint) {
                $constraint->aspectRatio();
            });
            // crop to 150x150img->crop(300, 175);
            $img->crop(245, 169);

            // if image width and heigth is equal to or greater than 150 and width is lesser than height
        } elseif ($width >= 245 && $height >= 169 && $width < $height) {
            // resize to 150
            $img->resize(245, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            // crop to 150x150
            $img->crop(245, 169);

            // if only image width is equal to or greater than 150
        } elseif ($width >= 245 && $height < 169) {
            // resize width to 150; height maybe shorter
            $img->resize(245, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            //$img->crop(245, 169);
            // if only image height is equal to or greater than 150
        } elseif ($width < 245 && $height >= 169) {
            // resize height to 150; width maybe shorter
            $img->resize(null, 169, function ($constraint) {
                $constraint->aspectRatio();
            });
            //$img->crop(245, 169);
        }

        // if none of the conditions are met (width and height are lesser than 150), no changes to the image will be made;
        return $img;
    }
}
if (!function_exists('resize_custom_image')){
    function resize_custom_image($img,$width,$height){	
        $img->resize($width, $height, function ($constraint) {
        $constraint->aspectRatio();
        });		
        return $img;
    }
}

if (!function_exists('setsession')){
    function setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$nPage,$sSessionName){	
        $aArray = [
          'search_str' => $sSearchStr,
          'search_by' => $sSearchBy,
          'order_field' => $sOrderField,
          'order_by' => $sOrderBy,
          'show_record' => $nShowRecord,
          'page_number' => $nPage,
        ];
        session([$sSessionName => $aArray]);
    }
}