<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransportPrice extends Model
{
     protected $table = 'ztransportprice';
    protected $fillable = [
                        'name', 'transport_id', 'minimum_pax','release','allotment','operates','passenger_type_id',
                        '[to]', '[from]', 'transport_base_price_id','transport_markup_percentage_id',
                        'transport_markup_id','transport_supplier_id','currency_id','cancellation_formula','cancellation_policy'
                        ];
    protected $primaryKey = 'id';
    
    public function currency(){
        return $this->hasOne('App\Currency','id','currency_id');
    }    

    public function supplier(){
        return $this->hasOne('App\TransportSupplier', 'id', 'transport_supplier_id');
    }
    
    public function passenger(){
        return $this->hasOne('App\TransportPassengerType','id','passenger_type_id');
    }
}
