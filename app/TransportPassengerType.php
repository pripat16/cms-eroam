<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransportPassengerType extends Model
{
    protected $fillable = [
        'name','code'
    ];
    protected $table = 'ztransporttypes';
    protected $primaryKey = 'id';
}
