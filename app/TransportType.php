<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransportType extends Model
{
    protected $fillable = [
        'name','icon','description','sequence','transport_mode'
    ];
    protected $table = 'ztransporttypes';
    protected $primaryKey = 'id';
}
