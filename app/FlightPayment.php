<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlightPayment extends Model
{
    protected $fillable = [
        'tour_id', 'season_id','flight_currency_id','flightPrice','flightDescription','flight_depart_city','isMandatory'
       
    ];
    protected $table = 'tblflightpayment';
    protected $primaryKey = 'flight_id';
    public $timestamps = false;
}
