<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class HotelSupplier extends Model
{
     protected $fillable = [
        'user_id','name','logo','description','abbreviation','product_contact_name','product_contact_email','product_contact_title',
        'product_contact_phone','reservation_contact_name', 'reservation_contact_email', 'reservation_contact_landline','reservation_contact_free_phone',
        'accounts_contact_name','accounts_contact_email','accounts_contact_title','accounts_contact_phone','special_notes','remarks','percentage'
    ];
    protected $table = 'zactivitysuppliers';
    protected $primaryKey = 'id';
    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
