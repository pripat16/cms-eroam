<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AECitiesMapped extends Model
{
    protected $fillable = [
        'ae_city_id','eroam_city_id'
    ];
    protected $table = 'zaecitiesmapped';
    protected $primaryKey = 'id';
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    public function ae_city()
    {
        return $this->hasOne('App\AECity', 'id', 'ae_city_id');
    }
}
