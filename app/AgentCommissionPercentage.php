<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class AgentCommissionPercentage extends Model {

    protected $table = 'zagentcommissionpercentages';
    protected $fillable = ['percentage','agent_id'];

    protected $dates = ['deleted_at'];    

}